# Adventure Company UI

* An app to track and distribute table-top rpg loot between player characters
* 3.0.0

## Contribution guidelines

### TypeScript Standards

* Fucntions should use PascalCase
* Variables should use camelCase
* Each logical component should be located in its own file
* Double quotations over single quotations whenever possible
* Tabs over spaces

### Upgrading Angular

`ng upgrade --all`
