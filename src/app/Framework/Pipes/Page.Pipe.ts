import { Pipe, PipeTransform } from "@angular/core";
import { ModelState } from "../../Financial/Models/ModelState";
import { PageEvent } from "@angular/material/paginator";

@Pipe({
	name: "Page"
})
export class Page implements PipeTransform
{
	public transform(items: ModelState[], pageEvent: PageEvent): ModelState[]
	{
		if (!items || !pageEvent)
		{
			return [];
		}

		const itemsToSkip = pageEvent.pageSize * pageEvent.pageIndex;

		return items.slice(itemsToSkip, pageEvent.length);
	}
}
