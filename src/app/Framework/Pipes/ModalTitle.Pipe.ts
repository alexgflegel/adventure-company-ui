﻿import { Pipe, PipeTransform } from "@angular/core";
import { Character, Encounter, Loot, Transaction } from "../../Financial/Models";

@Pipe({
	name: "ModalTitle"
})
export class ModalTitle implements PipeTransform
{
	public transform(template: object): string
	{
		if (!template)
		{
			return "";
		}

		return (<Character>template).name || (<Encounter>template).description || (<Loot>template).description || (<Transaction>template).description || "";
	}
}
