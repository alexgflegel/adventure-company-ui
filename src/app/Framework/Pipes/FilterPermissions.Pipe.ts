﻿import { Pipe, PipeTransform } from "@angular/core";
import { CompanyPermission } from "../../Financial/Models";

@Pipe({
	name: "FilterPermissions"
})
export class FilterPermissions implements PipeTransform
{
	/**
	 * Depricated: displays all non deleted items
	 * @param companyPermissions
	 */
	public transform(companyPermissions: CompanyPermission[]): CompanyPermission[]
	{
		if (!companyPermissions)
		{
			return [];
		}

		const result = companyPermissions.filter(item => !item.IsDeleted);

		return result;
	}
}
