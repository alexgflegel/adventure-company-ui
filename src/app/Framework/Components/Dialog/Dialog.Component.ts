import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

export interface DialogData
{
	type: string;
	name: string;
	description: string;
}

@Component({
	selector: "name-dialog-component",
	templateUrl: "Dialog.Component.html",
})
export class DialogComponent
{
	constructor(public dialogRef: MatDialogRef<DialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

	public Abort(): void
	{
		this.data.name = "";
		this.data.description = "";
		this.dialogRef.close();
	}
}
