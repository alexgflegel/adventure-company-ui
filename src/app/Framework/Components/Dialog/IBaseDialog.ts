export interface IBaseDialog
{
	/**
	 * Handles cancelling changes made to the model
	 */
	Abort(): void;

	/**
	 * Updates the model and triggering updates
	 */
	Accept(): void;

	/**
	 * Triggers the models delete function and closing the modal
	 */
	Delete(): void;
}
