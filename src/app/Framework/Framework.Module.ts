﻿import { NgModule } from "@angular/core";
import { MaximumValueDirective } from "./Directives/MaximumValue.Directive";
import { MinimumValueDirective } from "./Directives/MinimumValue.Directive";
import { OrRequiredDirective } from "./Directives/OrRequired.Directive";
import { FilterPermissions } from "./Pipes/FilterPermissions.Pipe";
import { ModalTitle } from "./Pipes/ModalTitle.Pipe";
import { Page } from "./Pipes/Page.Pipe";
import { DialogComponent } from "./Components/Dialog/Dialog.Component";
import { InputeFieldComponent } from "./Components/InputField/InputField.Component";
import { FormsModule } from "@angular/forms";
import { MaterialModule } from "../Material.Module";

const components = [
	DialogComponent,
	InputeFieldComponent,

	MinimumValueDirective,
	MaximumValueDirective,
	OrRequiredDirective,

	Page,
	ModalTitle,
	FilterPermissions
];

@NgModule({
	imports: [
		FormsModule,
		MaterialModule
	],
	declarations: [
		components
	],
	entryComponents: [
		DialogComponent
	],
	exports: [
		components
	]
})
export class FrameworkModule { }
