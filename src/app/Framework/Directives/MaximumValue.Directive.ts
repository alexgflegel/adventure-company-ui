import { Directive, Input, SimpleChanges, HostBinding, OnInit, OnChanges } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, Validator } from "@angular/forms";

@Directive({
	selector: "[MaximumValue]",
	providers: [{ provide: NG_VALIDATORS, useExisting: MaximumValueDirective, multi: true }]
})
export class MaximumValueDirective implements Validator, OnInit, OnChanges
{
	// input name must match the selector or the input will not be registered.
	/* tslint:disable-next-line:variable-name */
	@Input() public MaximumValue: number;
	@HostBinding("attr.max") public max: number;

	public ngOnInit(): void
	{
		if (this.MaximumValue || this.MaximumValue === 0)
		{
			this.max = this.MaximumValue;
		}
	}

	public ngOnChanges(changes: SimpleChanges): void
	{
		this.max = changes.MaximumValue.currentValue;
	}

	public validate(control: AbstractControl): { [key: string]: any } | null
	{
		return (control.value > this.MaximumValue) ? { "MaximumValue": { value: control.value } } : undefined;
	}
}
