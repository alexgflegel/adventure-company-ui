import { Directive, Input, SimpleChanges, OnChanges } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, Validator } from "@angular/forms";

@Directive({
	selector: "[OrRequired]",
	providers: [{ provide: NG_VALIDATORS, useExisting: OrRequiredDirective, multi: true }]
})
export class OrRequiredDirective implements Validator, OnChanges
{
	// input name must match the selector or the input will not be registered.
	/* tslint:disable-next-line:variable-name */
	@Input() public OrRequired: number;

	public ngOnChanges(changes: SimpleChanges): void
	{
		this.OrRequired = changes.OrRequired.currentValue;
	}

	/**
	 * This is a simple validator to display a manual entry error.
	 * @param control AbstractControl
	 */
	public validate(control: AbstractControl): { [key: string]: any } | null
	{
		const controlExists = !!control.value || control.value === 0;
		const requiredExists = !!this.OrRequired || this.OrRequired === 0;
		return !(controlExists || requiredExists) ? { "OrRequired": { value: true } } : undefined;
	}
}
