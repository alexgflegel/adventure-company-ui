import { Directive, Input, SimpleChanges, HostBinding, OnInit, OnChanges } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, Validator } from "@angular/forms";

@Directive({
	selector: "[MinimumValue]",
	providers: [{ provide: NG_VALIDATORS, useExisting: MinimumValueDirective, multi: true }]
})
export class MinimumValueDirective implements Validator, OnInit, OnChanges
{
	// input name must match the selector or the input will not be registered.
	/* tslint:disable-next-line:variable-name */
	@Input() public MinimumValue: number;
	@HostBinding("attr.min") public min: number;

	public ngOnInit(): void
	{
		if (this.MinimumValue || this.MinimumValue === 0)
		{
			this.min = this.MinimumValue;
		}
	}

	public ngOnChanges(changes: SimpleChanges): void
	{
		this.min = changes.MinimumValue.currentValue;
	}

	public validate(control: AbstractControl): { [key: string]: any } | null
	{
		return (control.value < this.MinimumValue) ? { "MinimumValue": { value: control.value } } : undefined;
	}
}
