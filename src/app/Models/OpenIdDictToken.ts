﻿export class OpenIdDictToken
{
	/* tslint:disable:variable-name */
	public access_token: string;
	public expires_in: number;
	public expiration_date: number;
	public id_token: string;
	public refresh_token: string;
	public token_type: string;
	/* tslint:enable */
}
