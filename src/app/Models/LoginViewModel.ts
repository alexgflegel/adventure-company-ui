﻿export class LoginViewModel
{
	public username: string;
	public password: string;
	/* tslint:disable-next-line:variable-name */
	public readonly grant_type = "password";
}
