﻿import { OpenIdDictToken } from "./OpenIdDictToken";

export class RefreshToken
{
	/* tslint:disable:variable-name */
	public refresh_token: string;
	public readonly grant_type = "refresh_token";
	/* tslint:enable */

	constructor(token: OpenIdDictToken)
	{
		this.refresh_token = token.refresh_token;
	}
}
