﻿import { Component, OnInit } from "@angular/core";
import { RegisterViewModel } from "../../Models";
import { AccountService } from "../../Services/Account.Service";
import { ToolbarService } from "../../Services/Toolbar.Service";

@Component({
	selector: "register-component",
	templateUrl: "Register.Component.html"
})
export class RegisterComponent implements OnInit
{
	public registerViewModel: RegisterViewModel;
	public errorMessage: string;

	constructor(private toolbarService: ToolbarService, private accountService: AccountService)
	{
		this.toolbarService.Title = "Register";
	}

	public ngOnInit(): void
	{
		this.registerViewModel = new RegisterViewModel();
	}

	public register(event: Event): void
	{
		event.preventDefault();

		this.accountService.Register(this.registerViewModel);
	}
}
