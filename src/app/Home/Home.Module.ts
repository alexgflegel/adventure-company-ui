﻿import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MaterialModule } from "../Material.Module";
import { ContactComponent, LoginComponent, MainComponent, RegisterComponent } from ".";

const appRoutes: Routes = [
	{ path: "", component: MainComponent, data: { title: "Home" } },
	{ path: "login", component: LoginComponent, data: { title: "Login" } },
	{ path: "register", component: RegisterComponent, data: { title: "Register" } },
	{ path: "contact", component: ContactComponent, data: { title: "Contact" } }
];

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes),
		FormsModule,
		CommonModule,
		MaterialModule
	],
	declarations: [MainComponent, ContactComponent, LoginComponent, RegisterComponent],
})
export class HomeModule { }
