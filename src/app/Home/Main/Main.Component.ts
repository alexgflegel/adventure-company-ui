﻿import { Component, OnInit } from "@angular/core";
import { Company } from "../../Financial/Models";
import { ToolbarService, } from "../../Services/Toolbar.Service";

@Component({
	selector: "main-index",
	templateUrl: "Main.Component.html"
})
export class MainComponent
{
	public company: Company;

	constructor(private tooblarService: ToolbarService)
	{
		this.tooblarService.Title = "Adventure Financial";
	}
}
