﻿import { Component, OnInit } from "@angular/core";
import { LoginViewModel } from "../../Models";
import { AuthenticationService } from "../../Services/Authentication.Service";
import { ToolbarService } from "../../Services/Toolbar.Service";

@Component({
	selector: "login-component",
	templateUrl: "Login.Component.html"
})
export class LoginComponent implements OnInit
{
	public loginViewModel: LoginViewModel;
	public errorMessage: string;

	constructor(private authenticationService: AuthenticationService, private toolbarService: ToolbarService)
	{
		this.toolbarService.Title = "Login";
	}

	public ngOnInit(): void
	{
		this.loginViewModel = new LoginViewModel();
	}

	/*
	* post the user's login details to server, if authenticated token is returned, then token is saved to session storage
	*/
	public Login(event: Event): void
	{
		event.preventDefault();

		this.authenticationService.Login(this.loginViewModel);
	}
}
