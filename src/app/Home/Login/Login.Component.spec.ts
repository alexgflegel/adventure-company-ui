import { async, TestBed, ComponentFixture } from "@angular/core/testing";
import { Component, ViewChild } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClient } from "@angular/common/http";

import { LoginComponent } from "./Login.Component";

describe("Login Component", () =>
{

	/**
	 * We need to create a host component that inflates our contact component.
	 *
	 * This component only needs to create and view the ContactFormComponent.
	 */
	@Component({
		selector: `host-component`,
		template: `<form #unit="ngForm"><contact-form [loginComponent]="unit" #loginComponent></contact-form></form>`
	})
	class TestHostComponent
	{
		@ViewChild("loginComponent", /* TODO: add static flag */ { static: false }) public loginComponent: LoginComponent;
	}

	let component: TestHostComponent;
	let componentFixture: ComponentFixture<TestHostComponent>;

	beforeEach(async(() =>
	{
		TestBed.configureTestingModule({
			declarations: [
				TestHostComponent
			],
			providers: [
				HttpClient
			],
			imports: [
				BrowserAnimationsModule,
				FormsModule,
				LoginComponent
			]
		}).compileComponents().then(() =>
		{
			componentFixture = TestBed.createComponent(TestHostComponent);
			component = componentFixture.componentInstance;

			// updates the component so that we can test changes after the @Input elements are set
			componentFixture.detectChanges();
		});
	}));

	it("should exist", async(() =>
	{
		// base objects should exist
		expect(component).toBeDefined();
	}));

	it("should be invalid by default", async(() =>
	{
	}));
});
