﻿import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { Router } from "@angular/router";
import { AuthenticationService } from "./Authentication.Service";
import { RegisterViewModel } from "../Models";
import { environment } from "../../environments/environment";

@Injectable()
export class AccountService
{
	private readonly url = `${ environment.apiUrl }/account/`;

	constructor(private http: HttpClient, private authenticationService: AuthenticationService, public router: Router) { }

	public GetUser(): Observable<HttpResponse<any>>
	{
		return this.http.get<HttpResponse<any>>(`${ this.url }GetUser`, { headers: this.authenticationService.AuthenticationJsonHeaders });
	}

	public Register(registerModel: RegisterViewModel): void
	{
		this.http.post<HttpResponse<any>>(`${ this.url }Register`,
			JSON.stringify(registerModel), { headers: this.authenticationService.JsonHeaders })
			.subscribe(response =>
			{
				if (response.ok)
				{
					this.router.navigate(["/login"]);
				}
				else
				{
					alert(response.body);
					console.log(response.body);
				}
			},
				error =>
				{
					// TODO: parse error messages, generate toast popups
					/* {
						"Email":["The Email field is required.","The Email field is not a valid e-mail address."],
						"Password":["The Password field is required.","The Password must be at least 6 characters long."]}
					*/
					alert(error.text);
					console.log(error.text);
				});
	}
}
