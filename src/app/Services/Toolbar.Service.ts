import { Injectable } from "@angular/core";
import { Title } from "@angular/platform-browser";

@Injectable()
export class ToolbarService
{
	private menuItems: MenuItem[];
	private title: string;
	private readonly home = "Adventure Financial";

	constructor(private titleService: Title)
	{
		this.Clear();
	}

	public get MenuItems(): MenuItem[]
	{
		return this.menuItems;
	}

	public Clear(): void
	{
		this.menuItems = [];
	}

	public Add(menuItem: MenuItem): void
	{
		this.menuItems.push(menuItem);
	}

	public set Title(title: string)
	{
		// updates the toolbar title
		this.title = title;
		// updates the tab title
		this.titleService.setTitle(this.MakeTitle(title));
	}

	public get Title(): string
	{
		return this.title;
	}

	/**
	 * Creates a formatted title
	 */
	private MakeTitle(title: string): string
	{
		if (title === this.home)
		{
			title = "Home";
		}

		return `${ this.home } - ${ title }`;
	}
}

export class MenuItem
{
	public path: string;
	public label: string;

	constructor(label: string, path: string)
	{
		this.label = label;
		this.path = path;
	}
}
