﻿import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router, CanActivate } from "@angular/router";
import { Observable, interval, Subscription, throwError } from "rxjs";
import { tap } from "rxjs/operators";
import { LogoutToken, OpenIdDictToken, LoginViewModel, RefreshToken } from "../Models";
import { TokenService } from "./Token.Service";
import { environment } from "../../environments/environment";

@Injectable()
export class AuthenticationService implements CanActivate
{
	private refreshSubscription: Subscription;
	private readonly url = `${ environment.apiUrl }/authorization/`;

	constructor(private http: HttpClient, public router: Router, private tokenManager: TokenService)
	{
		this.StartupTokenRefresh().pipe(tap(() => this.ScheduleRefresh()));
	}

	/**
	 * for requesting secure data using json
	 */
	public get AuthenticationJsonHeaders(): HttpHeaders
	{
		return new HttpHeaders()
			.append("Content-Type", "application/json")
			.append("Accept", "application/json")
			.append("Authorization", `Bearer ${ this.tokenManager.Tokens.access_token }`);
	}

	/**
	 * for requesting secure data from a form post
	 */
	public get AuthenticationFormHeaders(): HttpHeaders
	{
		return new HttpHeaders()
			.append("Content-Type", "application/x-www-form-urlencoded")
			.append("Accept", "application/json")
			.append("Authorization", `Bearer ${ this.tokenManager.Tokens.access_token }`);
	}

	/**
	 * for requesting unsecured data using json
	 */
	public get JsonHeaders(): HttpHeaders
	{
		return new HttpHeaders()
			.append("Content-Type", "application/json")
			.append("Accept", "application/json");
	}

	/**
	 * for requesting unsecured data using form post
	 */
	public get ContentHeaders(): HttpHeaders
	{
		return new HttpHeaders()
			.append("Content-Type", "application/x-www-form-urlencoded")
			.append("Accept", "application/json");
	}

	// simple check of logged in status: if there is a token, we're (probably) logged in.
	// ideally we check status and check token has not expired (server will back us up, if this not done, but it could be cleaner)
	public get IsLoggedIn(): boolean
	{
		return !!this.tokenManager.Tokens.access_token && !this.LoginExpired;
	}

	public get LoginExpired(): boolean
	{
		return (this.tokenManager.Tokens.expiration_date || new Date().getTime()) <= new Date().getTime();
	}

	public canActivate(): boolean
	{
		if (!this.IsLoggedIn)
		{
			this.router.navigate(["/login"]);
			return false;
		}
		return true;
	}

	public Login(login: LoginViewModel): void
	{
		this.GetRemoteTokens(login)
			.subscribe(() =>
			{
				this.ScheduleRefresh();
				this.router.navigate(["/company/"]);
			},
				error =>
				{
					// failed; TODO: add some nice toast / error handling
					alert(error.message);
					console.log(error.message);
				}
			);
	}

	public Logout(): void
	{
		this.http.get<LogoutToken>(`${ this.url }logout`, { headers: this.AuthenticationJsonHeaders })
			.subscribe(() => undefined,
				error =>
				{
					// failed; TODO: add some nice toast / error handling
					alert(error.message);
					console.log(error.message);
				}
			);

		// clear token in browser
		this.tokenManager.Clear();
		if (this.refreshSubscription)
		{
			this.refreshSubscription.unsubscribe();
		}
		// return to "home" page
		this.router.navigate(["/"]);
	}

	private GetRemoteTokens(data: RefreshToken | LoginViewModel): Observable<OpenIdDictToken>
	{
		const params = new URLSearchParams();
		// add the grant type to the model, with the appropriate scope
		Object.assign(data, { scope: "offline_access" });
		Object.keys(data).forEach(key => params.append(key, data[key]));

		return this.http.post<OpenIdDictToken>(`${ this.url }exchange`, params.toString(), { headers: this.ContentHeaders })
			.pipe(tap(token =>
			{
				this.tokenManager.Tokens = token;
			}));
	}

	private RefreshTokens(): Observable<OpenIdDictToken>
	{
		return this.GetRemoteTokens(new RefreshToken(this.tokenManager.Tokens));
	}

	private StartupTokenRefresh(): Observable<OpenIdDictToken>
	{
		const tokens = this.tokenManager.Tokens;

		if (!tokens.access_token)
		{
			return throwError("No token in Storage");
		}

		if (tokens.expiration_date <= new Date().getTime())
		{
			this.router.navigate(["/login"]);
		}

		return this.RefreshTokens();
	}

	private ScheduleRefresh(): void
	{
		// Refresh the tokens in half the expiration time.
		interval(this.tokenManager.Tokens.expires_in / 2 * 100).subscribe(() =>
		{
			this.refreshSubscription = this.RefreshTokens().subscribe();
		});
	}
}
