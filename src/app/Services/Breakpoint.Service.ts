import { Injectable } from "@angular/core";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";

@Injectable()
export class BreakpointService
{
	private isTabletSize = true;

	constructor(breakpointObserver: BreakpointObserver)
	{
		breakpointObserver.observe([
			Breakpoints.XLarge,
			Breakpoints.Large
		]).subscribe(result =>
		{
			if (result.matches)
			{
				this.isTabletSize = false;
			}
			else
			{
				this.isTabletSize = true;
			}
		});
	}

	public get IsTablet(): boolean
	{
		return this.isTabletSize;
	}
}
