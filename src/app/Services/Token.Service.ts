﻿import { OpenIdDictToken } from "../Models";
import { Injectable } from "@angular/core";

@Injectable()
export class TokenService
{
	private readonly tokenStore = "authentication-token";

	/**
	 * Called when logging out user; clears tokens from browser
	 */
	public Clear(): void
	{
		localStorage.removeItem(this.tokenStore);
	}

	/**
	 * After a successful login, save token data into session storage
	 */
	public set Tokens(token: OpenIdDictToken)
	{
		// note: use "localStorage" for persistent, browser-wide logins; "sessionStorage" for per-session storage.
		token.expiration_date = new Date().setSeconds(token.expires_in);
		token.refresh_token = token.refresh_token || this.Tokens.refresh_token;

		localStorage.setItem(this.tokenStore, JSON.stringify(token));
	}

	public get Tokens(): OpenIdDictToken
	{
		const tokensString = localStorage.getItem(this.tokenStore);
		return tokensString == null ? new OpenIdDictToken() : JSON.parse(tokensString);
	}
}
