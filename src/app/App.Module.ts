import { APP_BASE_HREF } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule, Title } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";

import { AccountService } from "./Services/Account.Service";
import { AuthenticationService } from "./Services/Authentication.Service";
import { BreakpointService } from "./Services/Breakpoint.Service";
import { CompanyService } from "./Financial/Services/Company.Service";
import { FinancialModule } from "./Financial/Financial.Module";
import { FrameworkModule } from "./Framework/Framework.Module";
import { HomeModule } from "./Home/Home.Module";
import { LayoutComponent } from "./Layout.Component";
import { MaterialModule } from "./Material.Module";
import { TokenService } from "./Services/Token.Service";
import { ToolbarService } from "./Services/Toolbar.Service";

// enableProdMode();

@NgModule({
	imports: [
		BrowserModule,
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		HomeModule,
		RouterModule,
		FrameworkModule,
		FinancialModule,
		MaterialModule
	],
	declarations: [
		LayoutComponent
	],
	providers: [
		CompanyService,
		AccountService,
		AuthenticationService,
		ToolbarService,
		TokenService,
		BreakpointService,
		Title,
		{ provide: APP_BASE_HREF, useValue: "/" }
	],
	bootstrap: [LayoutComponent]
})
export class AppModule { }
