import { Component, ViewChild } from "@angular/core";
import { AuthenticationService } from "./Services/Authentication.Service";
import { ToolbarService } from "./Services/Toolbar.Service";
import { Router } from "@angular/router";
import { MatSidenav } from "@angular/material/sidenav";
import { BreakpointService } from "./Services/Breakpoint.Service";

@Component({
	selector: "financial-app",
	styleUrls: ["Layout.Component.scss"],
	templateUrl: "Layout.Component.html"
})
export class LayoutComponent
{
	@ViewChild(MatSidenav, { static: false }) public sideNavigation: MatSidenav;

	constructor(
		private router: Router,
		private authenticationService: AuthenticationService,
		public toolbarService: ToolbarService,
		private breakpointService: BreakpointService
	) { }

	public get NavLayout(): string
	{
		return !this.breakpointService.IsTablet ? "side" : "over";
	}

	public get IsFixedNav(): boolean
	{
		return this.breakpointService.IsTablet;
	}

	/**
	 * Provide user's logged in status (do we have a token or not)
	 */
	public get IsLoggedIn(): boolean
	{
		return this.authenticationService.IsLoggedIn;
	}

	/**
	 * Clears token from server and locally in browser
	 */
	public Logout(): void
	{
		if (this.breakpointService.IsTablet)
		{
			this.sideNavigation.toggle();
		}

		// routes to home
		this.authenticationService.Logout();
	}

	public Navigate(path: string): void
	{
		// close the menu if in mobile
		if (this.breakpointService.IsTablet)
		{
			this.sideNavigation.toggle();
		}

		this.router.navigate([path]);
	}
}
