﻿import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { FrameworkModule } from "../Framework/Framework.Module";
import { MaterialModule } from "../Material.Module";
import { AuthenticationService } from "../Services/Authentication.Service";
import { CompanyService } from "./Services/Company.Service";

// Character Detail components
import { CharacterComponent, CharacterDetailComponent, CharacterDialogComponent, CharacterEncounterComponent, CharacterLootComponent } from "./Components/Character";
import { EncounterCharacterComponent, EncounterDetailComponent, EncounterComponent, EncounterLootComponent, EncounterDialogComponent } from "./Components/Encounter";
// Encounter Detail components
import { EncounterCharacterDialogComponent } from "./Components/EncounterCharacter";
import { LootDetailComponent, LootComponent, LootDialogComponent } from "./Components/Loot";
import { TransactionComponent, TransactionDialogComponent } from "./Components/Transaction";
// routed components
import { IndexComponent, ManageComponent, SettingsComponent } from "./Components";

import { CompanyDialogComponent } from "./Components/Company/CompanyDialog.Component";

import { CharacterTotal, FilterActiveCharacters, FilterAvailableCharacters, FilterByState, OrderCharacters, SumCharacterEncounter, SumCharacterTotal } from "./Pipes/Character";
import { AutoCompleteEncounter, FilterAvailableEncounters, OrderEncounterCharacters, OrderEncounters, SumEncounterLoot, SumEncounterShares, SumEncounterTransactions, SumEncounterCharacter } from "./Pipes/Encounter";
import { FilterClaimedLoot, FilterAvailableLoots, OrderLoot, SumRemainingLoot, SumLootValue } from "./Pipes/Loot";
import { FilterDeleted } from "./Pipes";
import { FilterLootTransactions, GatherLootTransactions, SumTransactionAmounts, SumTransactionQuantity } from "./Pipes/Transaction";

const characterDetailComponents = [CharacterDetailComponent, CharacterLootComponent, CharacterEncounterComponent, CharacterDialogComponent, EncounterCharacterDialogComponent];
const encounterDetailComponents = [EncounterDetailComponent, EncounterCharacterComponent, EncounterLootComponent, EncounterDialogComponent];
const financialPipes = [AutoCompleteEncounter, CharacterTotal, FilterActiveCharacters, FilterAvailableCharacters, FilterAvailableEncounters,
	FilterAvailableLoots, FilterByState, FilterClaimedLoot, FilterDeleted, FilterLootTransactions, GatherLootTransactions,
	OrderCharacters, OrderEncounterCharacters, OrderEncounters, OrderLoot, SumCharacterEncounter, SumCharacterTotal,
	SumEncounterCharacter, SumEncounterLoot, SumEncounterShares, SumEncounterTransactions, SumLootValue, SumRemainingLoot,
	SumTransactionAmounts, SumTransactionQuantity];

const appRoutes: Routes = [
	{ path: "company", component: IndexComponent, data: { title: "Companies" }, canActivate: [AuthenticationService] },
	{
		path: "company/:id", component: ManageComponent, data: { title: "Manage" }, canActivate: [AuthenticationService],
		children: [
			{ path: "settings", component: SettingsComponent, data: { title: "Settings" }, canActivate: [AuthenticationService] }
		]
	}
];

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes),
		FormsModule,
		CommonModule,
		MaterialModule,
		FrameworkModule
	],
	declarations: [
		CharacterComponent,
		characterDetailComponents,
		CompanyDialogComponent,
		EncounterComponent,
		encounterDetailComponents,
		EncounterDialogComponent,
		financialPipes,
		IndexComponent,
		LootComponent,
		LootDetailComponent,
		LootDialogComponent,
		ManageComponent,
		SettingsComponent,
		TransactionComponent,
		TransactionDialogComponent
	],
	providers: [
		CompanyService
	],
	entryComponents: [
		CharacterDialogComponent,
		EncounterCharacterDialogComponent,
		EncounterDialogComponent,
		LootDialogComponent,
		TransactionDialogComponent
	],
	exports: [
		CharacterComponent,
		characterDetailComponents,
		EncounterComponent,
		encounterDetailComponents,
		financialPipes,
		IndexComponent,
		LootComponent,
		LootDetailComponent,
		LootDialogComponent,
		ManageComponent,
		SettingsComponent,
		TransactionComponent,
		TransactionDialogComponent
	]
})
export class FinancialModule { }
