﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { Company, CompanyIndex } from "../Models";
import { AuthenticationService } from "../../Services/Authentication.Service";

@Injectable()
export class TransactionService
{
	private readonly url: string = "company/";

	constructor(private http: HttpClient, private authenticationService: AuthenticationService) { }

	public Get(id: number): Observable<Company>
	{
		return this.http.get<Company>(this.GetUrl(id),
			{ headers: this.authenticationService.AuthenticationJsonHeaders })
			.pipe(map(resp => new Company(resp)));
	}

	public GetIndex(): Observable<CompanyIndex>
	{
		return this.http.get<CompanyIndex>(this.url,
			{ headers: this.authenticationService.AuthenticationJsonHeaders })
			.pipe(map(resp => resp as CompanyIndex));
	}

	public Update(company: Company): Observable<Company>
	{
		return this.http
			.put<Company>(this.GetUrl(company.companyId),
				JSON.stringify(company),
				{ headers: this.authenticationService.AuthenticationJsonHeaders })
			.pipe(map(resp => new Company(resp)));
	}

	public Add(company: Company): Observable<Company>
	{
		return this.http
			.post<Company>(this.url,
				JSON.stringify(company),
				{ headers: this.authenticationService.AuthenticationJsonHeaders })
			.pipe(map(resp => new Company(resp)));
	}

	/* tslint:disable-next-line:variable-name */
	private GetUrl = (companyId: number | string): string => `${ this.url }${ companyId }`;
}
