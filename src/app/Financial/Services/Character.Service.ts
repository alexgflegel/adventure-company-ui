﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Character } from "../Models";
import { AuthenticationService } from "../../Services/Authentication.Service";

@Injectable()
export class CharacterService
{
	private readonly url: string = "character/";

	constructor(private http: HttpClient, private authenticationService: AuthenticationService) { }

	public Get(id: number): Observable<Character>
	{
		return this.http.get<Character>(this.GetUrl(id),
			{ headers: this.authenticationService.AuthenticationJsonHeaders });
	}

	public Update(company: Character): Observable<Character>
	{
		return this.http
			.put<Character>(this.GetUrl(+company.companyId),
				JSON.stringify(company),
				{ headers: this.authenticationService.AuthenticationJsonHeaders });
	}

	public Add(company: Character): Observable<Character>
	{
		return this.http
			.post<Character>(this.url,
				JSON.stringify(company),
				{ headers: this.authenticationService.AuthenticationJsonHeaders });
	}

	/* tslint:disable-next-line:variable-name */
	private GetUrl = (companyId: number): string => `${ this.url }${ companyId }`;
}
