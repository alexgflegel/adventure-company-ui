import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { IBaseDialog } from "../../../Framework/Components/Dialog/IBaseDialog";
import { Company } from "../../Models";

export interface CompanyDialogData
{
	model: Company;
	create: boolean;
}

@Component({
	selector: "financial-company-dialog",
	templateUrl: "CompanyDialog.Component.html",
})
export class CompanyDialogComponent implements IBaseDialog
{
	public company: Company;

	constructor(public dialogRef: MatDialogRef<CompanyDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: CompanyDialogData)
	{
		this.company = Object.create(this.data.model);
	}

	public Abort(): void
	{
		this.dialogRef.close();
	}

	public Accept(): void
	{
		this.data.model.Update(this.company);
		this.dialogRef.close(this.data.model);
	}

	public Delete(): void
	{
		this.data.model.Delete();
		this.dialogRef.close();
	}
}
