import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { CompanyService } from "../../Services/Company.Service";
import { Company, CompanyIndex } from "../../Models";
import { MatDialog } from "@angular/material/dialog";
import { DialogComponent, DialogData } from "../../../Framework/Components/Dialog/Dialog.Component";
import { ToolbarService } from "../../../Services/Toolbar.Service";

@Component({
	selector: "financial-index",
	styleUrls: ["Index.Component.scss"],
	templateUrl: "Index.Component.html"
})
export class IndexComponent implements OnInit
{
	public companyList: Array<Company>;
	public companyListRel: Array<Company>;
	public errorMessage: string;

	constructor(public router: Router, private companyService: CompanyService, private dialog: MatDialog, private toolbarService: ToolbarService) { }

	public ngOnInit(): void
	{
		this.Get();
		this.toolbarService.Title = "Adventure Companies";
	}

	public Load(company: Company): void
	{
		this.router.navigate([`/company/${ company.companyId }/`]);
	}

	public Add(event: Event): void
	{
		event.preventDefault();

		const dialogRef = this.dialog.open(DialogComponent, {
			panelClass: "dialog-fill",
			data: { name: "", type: "Company" }
		});

		dialogRef.afterClosed().subscribe((result: DialogData) =>
		{
			if (result)
			{
				// add the new company
				let company = new Company();
				company.name = result.name;
				company.description = result.description;
				company.companyId = -1;
				company.PrepareSave();

				this.companyService.Add(company).subscribe(
					(data: Company) => company = data,
					error => this.errorMessage = <any>error.message,
					() => this.Put(company));
			}
		});
	}

	private Put(data: Company): void
	{
		this.companyList.push(data);
	}

	private Get(): void
	{
		let companyLists: CompanyIndex;

		this.companyService.GetIndex().subscribe(
			(data: CompanyIndex) => companyLists = data,
			error => this.errorMessage = <any>error.message,
			() => this.Set(companyLists));
	}

	private Set(data: CompanyIndex): void
	{
		this.companyList = data.myCompanies;
		this.companyListRel = data.sharedCompanies;
	}
}
