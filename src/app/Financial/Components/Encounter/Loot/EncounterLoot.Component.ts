﻿import { Component, Input } from "@angular/core";

import { Encounter, Loot } from "../../../Models";

@Component({
	selector: "financial-encounter-loot",
	templateUrl: "EncounterLoot.Component.html",
	styleUrls: ["../../../../Framework/Styles/accordion.scss"]
})
export class EncounterLootComponent
{
	@Input() public encounter: Encounter;

	public Add(event: Event): void
	{
		event.preventDefault();

		const loot = new Loot(this.encounter);
		this.encounter.loots.push(loot);
		loot.Dirty();
	}

	public Load(event: Event, loot: Loot): void
	{
		event.preventDefault();
	}
}
