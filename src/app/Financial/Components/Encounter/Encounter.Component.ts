import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Company, Encounter } from "../../Models";
import { EncounterDialogComponent } from "./Detail";

@Component({
	selector: "financial-company-encounter",
	templateUrl: "Encounter.Component.html",
	styleUrls: ["../../../Framework/Styles/accordion.scss"]
})
export class EncounterComponent
{
	@Input() public company: Company;

	constructor(private dialog: MatDialog) { }

	public Load(encounter: Encounter): void
	{
		encounter.Dirty();
	}

	public Add(event: Event): void
	{
		event.preventDefault();

		const dialogRef = this.dialog.open(EncounterDialogComponent, {
			panelClass: "dialog-fill",
			data: { model: new Encounter(this.company), create: true }
		});

		dialogRef.afterClosed().subscribe((result: Encounter) =>
		{
			if (result)
			{
				// add the new encounter
				const newEncounter = new Encounter(this.company);
				newEncounter.description = result.description;
				this.company.encounters.push(newEncounter);
				newEncounter.Dirty();
			}
		});
	}
}
