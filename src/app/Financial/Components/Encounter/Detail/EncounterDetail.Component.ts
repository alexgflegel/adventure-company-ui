import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Encounter } from "../../../Models";
import { EncounterDialogComponent } from "./EncounterDialog.Component";

@Component({
	selector: "financial-encounter-detail",
	templateUrl: "EncounterDetail.Component.html"
})
export class EncounterDetailComponent
{
	@Input() public encounter: Encounter;

	constructor(private dialog: MatDialog) { }

	public Edit(): void
	{
		this.dialog.open(EncounterDialogComponent, {
			panelClass: "dialog-fill",
			data: { model: this.encounter, create: false }
		});
	}
}
