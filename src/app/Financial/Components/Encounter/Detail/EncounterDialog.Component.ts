import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Encounter } from "../../../Models";
import { IBaseDialog } from "../../../../Framework/Components/Dialog/IBaseDialog";

export class EncounterDialogData
{
	public model: Encounter;
	public create: boolean;
}

@Component({
	selector: "financial-encounter-dialog",
	templateUrl: "EncounterDialog.Component.html",
	styles: [
		".mat-step-label.mat-step-label-active { flex-grow: 1; }"
	]
})
export class EncounterDialogComponent implements IBaseDialog
{
	public encounter: Encounter;

	constructor(public dialogRef: MatDialogRef<EncounterDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: EncounterDialogData)
	{
		this.encounter = Object.create(this.data.model);
	}

	public Abort(): void
	{
		this.dialogRef.close();
	}

	public Accept(): void
	{
		this.data.model.Update(this.encounter);
		this.dialogRef.close(this.data.model);
	}

	public Delete(): void
	{
		this.data.model.Delete();
		this.dialogRef.close();
	}
}
