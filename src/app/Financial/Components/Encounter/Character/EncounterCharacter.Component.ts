import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Encounter, EncounterCharacter } from "../../../Models";
import { FilterActiveCharacters } from "../../../Pipes/Character";
import { EncounterCharacterDialogComponent } from "../../EncounterCharacter";

@Component({
	selector: "financial-encounter-character",
	templateUrl: "EncounterCharacter.Component.html"
})
export class EncounterCharacterComponent
{
	@Input() public encounter: Encounter;

	constructor(private dialog: MatDialog) { }

	public get AddDisabled(): boolean
	{
		const activeCharacters = new FilterActiveCharacters().transform(this.encounter.company.characters);

		const encounterCharacters = this.encounter.encounterCharacters.filter(f => !f.IsDeleted).map(map => map.character);

		if (encounterCharacters.filter(item => !item).length)
		{
			return true;
		}

		// if there are more active characters than
		if (activeCharacters.length > this.encounter.encounterCharacters.length)
		{
			return false;
		}

		// check to see if there are any available characters not assigned to this encounter
		activeCharacters.map(item =>
		{
			if (!encounterCharacters.find(existing => existing.characterId === item.characterId))
			{
				// if there is a missing active character add is not disabled
				return false;
			}
		});
		return true;
	}

	public Add(event: Event): void
	{
		event.preventDefault();

		if (this.AddDisabled)
		{
			return;
		}

		const dialogRef = this.dialog.open(EncounterCharacterDialogComponent, {
			panelClass: "dialog-fill",
			data: { model: new EncounterCharacter(), create: true, encounter: this.encounter }
		});

		const subscription = dialogRef.afterClosed().subscribe((result?: EncounterCharacter) =>
		{
			if (result)
			{
				const newTransaction = new EncounterCharacter(this.encounter);

				newTransaction.characterId = result.characterId;
				newTransaction.encounterId = result.encounterId;
				newTransaction.shares = result.shares;

				newTransaction.encounter = result.encounter;
				newTransaction.character = result.character;

				// trigger loot-character connection
				newTransaction.UpdateCharacter();
				newTransaction.UpdateEncounter();

				newTransaction.Dirty();
			}
			else
			{
				subscription.unsubscribe();
			}
		});
	}

	public AddAll(event: Event): void
	{
		event.preventDefault();

		if (this.AddDisabled)
		{
			return;
		}

		const characterFilter = new FilterActiveCharacters();
		const activeCharacters = characterFilter.transform(this.encounter.company.characters);

		// TODO: Cleanup
		activeCharacters.map(item =>
		{
			// skip any character who has a non-deleted EncounterCharacter record
			// even if they have a deleted EncounterCharacter record
			if (!this.encounter.encounterCharacters.find(existing => existing.characterId === item.characterId && !existing.IsDeleted))
			{
				const emptyEncounterCharacter = this.encounter.encounterCharacters.find(map => !map.character);
				const deletedEncounterCharacter = this.encounter.encounterCharacters.find(existing => existing.characterId === item.characterId && existing.IsDeleted);

				// if their record is deleted
				if (deletedEncounterCharacter)
				{
					deletedEncounterCharacter.Undelete();
				}
				// if there is an empty record, use it
				else if (emptyEncounterCharacter)
				{
					// if an existing record is found, but is empty
					emptyEncounterCharacter.character = item;
					emptyEncounterCharacter.characterId = item.characterId;
					emptyEncounterCharacter.Undelete();

					item.encounterCharacters.push(emptyEncounterCharacter);
				}
				// if there is no existing records to use create a new one.
				else
				{
					const encounterCharacter = new EncounterCharacter(this.encounter, item);
					this.encounter.encounterCharacters.push(encounterCharacter);
					encounterCharacter.Dirty();
				}
			}
		});
	}

	public Edit(encounterCharacter: EncounterCharacter): void
	{
		this.dialog.open(EncounterCharacterDialogComponent, {
			panelClass: "dialog-fill",
			data: { model: encounterCharacter, create: false }
		});
	}
}
