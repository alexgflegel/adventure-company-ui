import { Component, Input, OnInit } from "@angular/core";
import { Company } from "../../Models";
import { ActivatedRoute } from "@angular/router";
import { CompanyService } from "../../Services/Company.Service";

@Component({
	selector: "financial-company-settings",
	templateUrl: "Settings.Component.html"
})
export class SettingsComponent implements OnInit
{
	@Input() public company: Company;

	constructor(public route: ActivatedRoute, private companyService: CompanyService) { }

	public ngOnInit(): void
	{
		this.route.params.subscribe(params =>
		{
			this.Get(+params.id); // (+) converts string "id" to a number
		});
	}

	private Get(id: number): void
	{
		let subscribedData: any;
		this.companyService.Get(id)
			.subscribe((data: Company) => subscribedData = data,
				() => this.SyncData(subscribedData));
	}

	/**
	 * Updates the page data with a new company record
	 */
	private SyncData(data: Company): void
	{
		this.company = data;
	}
}
