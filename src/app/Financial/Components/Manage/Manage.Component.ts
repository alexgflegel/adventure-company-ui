import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { CompanyService } from "../../Services/Company.Service";

import { Company, Encounter } from "../../Models";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatTabGroup } from "@angular/material/tabs";
import { ToolbarService } from "../../../Services/Toolbar.Service";

@Component({
	selector: "financial-company",
	templateUrl: "Manage.Component.html"
})
export class ManageComponent implements OnInit
{
	@ViewChild(MatTabGroup, { static: false }) public matTab: MatTabGroup;

	public company: Company;
	public errorMessage: string;

	constructor(public route: ActivatedRoute, private companyService: CompanyService, private snackBar: MatSnackBar, private toolbarService: ToolbarService) { }

	public ngOnInit(): void
	{
		this.route.params.subscribe(params =>
		{
			this.Get(+params.id); // (+) converts string "id" to a number
		});
	}

	public Update(event: Event): void
	{
		event.preventDefault();

		if (!this.company)
		{
			return;
		}

		// effectively clones the the company data into a separate variable.
		const test = new Company(this.company);

		test.PrepareSave();

		let subscribedData: any;

		this.companyService
			.Update(test)
			.subscribe((data: Company) => (subscribedData = data), error => (this.errorMessage = <any>error.message), () => this.SyncData(subscribedData));
	}

	public Load(event: Encounter): void
	{
		// event.preventDefault();
		this.matTab.selectedIndex = 1;
	}

	private Get(id: number): void
	{
		let subscribedData: any;
		this.companyService
			.Get(id)
			.subscribe((data: Company) => (subscribedData = data), error => (this.errorMessage = <any>error.message), () => this.SyncData(subscribedData));
	}

	private OpenSnackBar(message: string): void
	{
		this.snackBar.open(message, "", {
			duration: 2000
		});
	}

	private SyncData(data: Company): void
	{
		this.company = data;
		this.OpenSnackBar("Company Synced");
		this.toolbarService.Title = this.company.name;
	}
}
