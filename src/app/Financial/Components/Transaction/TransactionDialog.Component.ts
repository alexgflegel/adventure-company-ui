import { Component, ViewChild, ChangeDetectorRef, Inject } from "@angular/core";
import { Character, CharacterDisplay, Transaction, TransactionMode } from "../../Models";
import { Observable } from "rxjs";
import { NgModel } from "@angular/forms";
import { AutoCompleteCharacter } from "../../Pipes/Character";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { IBaseDialog } from "../../../Framework/Components/Dialog/IBaseDialog";

export interface TransactionDialogData
{
	mode: TransactionMode;
	model: Transaction;
	character: Character;
	create: boolean;
}

@Component({
	selector: "financial-transaction-dialog",
	templateUrl: "TransactionDialog.Component.html"
})
export class TransactionDialogComponent implements IBaseDialog
{
	public mode: TransactionMode = "Encounter";
	public transaction: Transaction;
	public filteredCharacters: Observable<Character[]>;
	public CharacterDisplay: Function = CharacterDisplay;
	private character: NgModel;

	@ViewChild("character", { static: false }) set Character(character: NgModel)
	{
		this.character = character;

		if (this.character && !this.filteredCharacters)
		{
			this.filteredCharacters = new AutoCompleteCharacter().transform(this.character, this.transaction.loot.encounter.company.characters, this.transaction.character);
			this.changeDetector.detectChanges();
		}
	}

	constructor(public dialogRef: MatDialogRef<TransactionDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: TransactionDialogData, private changeDetector: ChangeDetectorRef)
	{
		this.transaction = Object.create(this.data.model);

		if (this.data.character)
		{
			this.transaction.character = this.data.character;
		}

		this.mode = this.data.mode;
	}

	public EmptyClear(): void
	{
		if (!this.transaction.character || typeof this.transaction.character === "string")
		{
			this.transaction.Dirty();
		}
	}

	public Abort(): void
	{
		this.dialogRef.close();
	}

	public Accept(): void
	{
		// update existing character if needed
		this.transaction.UpdateCharacter();
		this.data.model.Update(this.transaction);
		this.dialogRef.close(this.data.model);
	}

	public Delete(): void
	{
		this.data.model.Delete();
		this.dialogRef.close();
	}
}
