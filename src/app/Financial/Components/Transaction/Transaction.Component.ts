import { Component, OnInit, Input } from "@angular/core";

import { Character, Encounter, Transaction, TransactionMode, Loot } from "../../Models";
import { SumRemainingLoot } from "../../Pipes/Loot";
import { TransactionDialogComponent } from "./TransactionDialog.Component";
import { MatDialog } from "@angular/material/dialog";

@Component({
	selector: "financial-transaction",
	templateUrl: "Transaction.Component.html"
})
export class TransactionComponent implements OnInit
{
	@Input() public encounter: Encounter;
	@Input() public character: Character;
	@Input() public loot: Loot;

	public mode: TransactionMode;
	public transactions: Transaction[];

	constructor(private dialog: MatDialog) { }

	public ngOnInit(): void
	{
		this.transactions = [];

		if (this.encounter)
		{
			this.mode = "Encounter";
		}
		else if (this.loot)
		{
			// in the event that there is both a character and loot choose loot over characters
			this.mode = "Loot";
		}
		else if (this.character)
		{
			this.mode = "Character";
		}
	}

	public Add(event: Event): void
	{
		event.preventDefault();

		// new loot needs to be added to loot and character if applicable
		const dialogRef = this.dialog.open(TransactionDialogComponent, {
			panelClass: "dialog-fill",
			data: { create: false, model: new Transaction(this.encounter, this.loot), mode: this.mode, character: this.character }
		});

		const subscription = dialogRef.afterClosed().subscribe((result?: Transaction) =>
		{
			if (result)
			{
				const newTransaction = new Transaction();

				newTransaction.description = result.description;
				newTransaction.amount = result.amount;
				newTransaction.quantity = result.quantity;
				newTransaction.characterId = result.characterId;
				newTransaction.lootId = result.lootId;
				newTransaction.encounterId = result.encounterId;

				newTransaction.encounter = result.encounter;
				newTransaction.loot = result.loot;
				newTransaction.character = result.character;

				// add the new encounterCharacter
				if (newTransaction.encounter)
				{
					newTransaction.encounter.transactions.push(newTransaction);
				}

				if (newTransaction.loot)
				{
					newTransaction.loot.transactions.push(newTransaction);
				}

				// trigger loot-character connection
				newTransaction.UpdateCharacter();

				newTransaction.Dirty();
			}
			else
			{
				subscription.unsubscribe();
			}
		});

		// trigger the pure pipe update
		this.transactions = [];
	}

	public get AddDisabled(): boolean
	{
		const lootSummary = new SumRemainingLoot();

		if (this.mode === "Loot" && !lootSummary.transform(this.loot))
		{
			return true;
		}

		return false;
	}

	public Edit(transaction: Transaction): void
	{
		this.dialog.open(TransactionDialogComponent, {
			panelClass: "dialog-fill",
			data: { create: false, model: transaction, mode: this.mode }
		});
	}
}
