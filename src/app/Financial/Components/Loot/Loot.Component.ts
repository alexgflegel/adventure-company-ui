﻿import { Component, OnInit, Input } from "@angular/core";
import { Company, Loot } from "../../Models";

export type LootMode = "Available" | "All";

@Component({
	selector: "financial-company-loot",
	templateUrl: "Loot.Component.html",
	styleUrls: ["../../../Framework/Styles/accordion.scss"]
})
export class LootComponent implements OnInit
{
	@Input() public company: Company;

	public searchState: LootMode;

	public ngOnInit(): void
	{
		this.searchState = "Available";
	}

	public Load(event: Event, loot: Loot): void
	{
		event.preventDefault();
	}

	public Switch(lootMode: LootMode): void
	{
		this.searchState = lootMode;
	}

	public GetActive(lootMode: LootMode): string
	{
		if (this.searchState === lootMode)
		{
			return "active";
		}
		else
		{
			return "";
		}
	}
}
