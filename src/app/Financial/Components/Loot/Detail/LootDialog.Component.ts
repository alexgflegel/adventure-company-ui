import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Loot } from "../../../Models";

export interface LootDialogData
{
	model: Loot;
	create: boolean;
}

@Component({
	selector: "financial-loot-dialog",
	templateUrl: "LootDialog.Component.html"
})
export class LootDialogComponent
{
	public loot: Loot;

	constructor(public dialogRef: MatDialogRef<LootDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: LootDialogData)
	{
		this.loot = Object.create(this.data.model);
	}

	public Abort(): void
	{
		this.dialogRef.close();
	}

	public Accept(): void
	{
		this.data.model.Update(this.loot);
		this.dialogRef.close(this.data.model);
	}

	public Delete(): void
	{
		this.data.model.Delete();
		this.dialogRef.close();
	}
}
