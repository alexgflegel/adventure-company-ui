import { Component, Input } from "@angular/core";
import { Loot } from "../../../Models";
import { LootDialogComponent } from "./LootDialog.Component";
import { MatDialog } from "@angular/material/dialog";

@Component({
	selector: "financial-loot-detail",
	templateUrl: "LootDetail.Component.html"
})
export class LootDetailComponent
{
	@Input() public loot: Loot;

	constructor(private dialog: MatDialog) { }

	public Edit(): void
	{
		this.dialog.open(LootDialogComponent, {
			panelClass: "dialog-fill",
			data: { model: this.loot, create: false }
		});
	}
}
