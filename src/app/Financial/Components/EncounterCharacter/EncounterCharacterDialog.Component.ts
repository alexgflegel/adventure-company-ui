import { Component, Inject, ViewChild, ChangeDetectorRef } from "@angular/core";
import { Character, CharacterDisplay, EncounterDisplay, Encounter, EncounterCharacter } from "../../Models";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Observable } from "rxjs";
import { AutoCompleteEncounterCharacter, AutoCompleteEncounter, SumEncounterShares, SumEncounterTransactions } from "../../Pipes/Encounter";
import { NgModel } from "@angular/forms";
import { DecimalPipe } from "@angular/common";
import { IBaseDialog } from "../../../Framework/Components/Dialog/IBaseDialog";

export interface EncounterCharacterDialogData
{
	model: EncounterCharacter;
	character: Character;
	encounter: Encounter;
	create: boolean;
}

@Component({
	selector: "financial-encounter-character-dialog",
	templateUrl: "EncounterCharacterDialog.Component.html",
})
export class EncounterCharacterDialogComponent implements IBaseDialog
{
	public encounterCharacter: EncounterCharacter;
	public filteredCharacters: Observable<Character[]>;
	public filteredEncounters: Observable<Encounter[]>;
	public CharacterDisplay: Function = CharacterDisplay;
	public EncounterDisplay: Function = EncounterDisplay;
	public lazyLoad = false;
	private originalShare = 0;
	private character: NgModel;
	private encounter: NgModel;

	@ViewChild("encounter", { static: false }) set Encounter(encounter: NgModel)
	{
		this.encounter = encounter;

		if (this.encounter && !this.filteredEncounters)
		{
			this.filteredEncounters = new AutoCompleteEncounter().transform(this.encounter, this.encounterCharacter);
			this.changeDetector.detectChanges();
		}
	}

	@ViewChild("character", { static: false }) set Character(character: NgModel)
	{
		this.character = character;

		if (this.character && !this.filteredCharacters)
		{
			this.filteredCharacters = new AutoCompleteEncounterCharacter().transform(this.character, this.encounterCharacter);
			this.changeDetector.detectChanges();
		}
	}

	constructor(public dialogRef: MatDialogRef<EncounterCharacterDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: EncounterCharacterDialogData, private changeDetector: ChangeDetectorRef)
	{
		this.encounterCharacter = Object.create(this.data.model);
		this.originalShare = this.encounterCharacter.shares;

		if (data.encounter)
		{
			this.encounterCharacter.encounter = this.data.encounter;
		}

		this.lazyLoad = true;
	}

	public ClearCharacter(): void
	{
		if (!this.encounterCharacter.character || typeof this.encounterCharacter.character === "string")
		{
			this.encounterCharacter.Dirty();
		}
	}

	public ClearEncounter(): void
	{
		if (!this.encounterCharacter.encounter || typeof this.encounterCharacter.encounter === "string")
		{
			this.encounterCharacter.Dirty();
		}
	}

	public Remove(event: Event): void
	{
		event.preventDefault();
		this.encounterCharacter.Delete();
	}

	public Abort(): void
	{
		this.dialogRef.close();
	}

	public Accept(): void
	{
		this.encounterCharacter.UpdateCharacter();
		this.encounterCharacter.UpdateEncounter();
		this.data.model.Update(this.encounterCharacter);
		this.dialogRef.close(this.data.model);
	}

	public Delete(): void
	{
		this.data.model.Delete();
		this.dialogRef.close();
	}

	/**
	 * Adjusts the total shares by removing the original share and adding the modidified shares
	 */
	public get AdjustedShare(): number
	{
		return (new SumEncounterShares().transform(this.encounterCharacter.encounter)) - this.originalShare + this.encounterCharacter.shares;
	}

	/**
	 * Returns the share percentage formatted
	 */
	public get PercentageShare(): string
	{
		return `${ new DecimalPipe("en-us").transform(this.SharePercentage * 100, "1.2-2") }%`;
	}

	/**
	 * Calculates the share value using the adjusted share percentage
	 */
	public get ShareValue(): string
	{
		return `${ new DecimalPipe("en-us").transform(new SumEncounterTransactions().transform(this.encounterCharacter.encounter) * this.SharePercentage, "1.2-2") }`;
	}

	/**
	 * Calculates the percentage of shares using the adjusted share count
	 */
	private get SharePercentage(): number
	{
		return this.encounterCharacter.shares / this.AdjustedShare;
	}
}
