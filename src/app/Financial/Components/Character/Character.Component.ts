﻿import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { Character, CharacterState, Company, Encounter } from "../../Models";
import { MatDialog, MatPaginator, PageEvent } from "@angular/material";
import { CharacterDialogComponent } from "./Detail";

@Component({
	selector: "financial-company-character",
	templateUrl: "Character.Component.html",
	styleUrls: ["../../../Framework/Styles/accordion.scss"]
})
export class CharacterComponent implements OnInit
{
	@Input() public company: Company;
	@Output() public navigationCallback: EventEmitter<any> = new EventEmitter();
	@ViewChild(MatPaginator, { static: false }) public pageinator: MatPaginator;

	public characterStates = CharacterState;
	public searchState: CharacterState;
	public pageData = new PageEvent();

	constructor(private dialog: MatDialog)
	{
		this.pageData.length = 10;
		this.pageData.pageIndex = 0;
	}

	public ngOnInit(): void
	{
		this.searchState = CharacterState.Active;
	}

	public UpdatePageData(pageEvent: PageEvent): void
	{
		this.pageData = pageEvent;
	}

	public Add(event: Event): void
	{
		event.preventDefault();
		const dialogRef = this.dialog.open(CharacterDialogComponent, {
			panelClass: "dialog-fill",
			data: { model: new Character(this.company), create: true }
		});

		// subscribe to the closing event to create a new character with the dialog result
		dialogRef.afterClosed().subscribe((result: Character) =>
		{
			if (result)
			{
				// add the new character
				const character = new Character(this.company);
				character.name = result.name;
				character.description = result.description;
				character.status = result.status;

				this.company.characters.push(character);
				character.Dirty();
			}
		});
	}

	public Load(event: Encounter): void
	{
		this.navigationCallback.emit(event);
	}

	public Switch(characterState: CharacterState): void
	{
		this.searchState = characterState;
	}

	public GetActive(characterState: CharacterState): string
	{
		return this.searchState === characterState ? "active" : "";
	}
}
