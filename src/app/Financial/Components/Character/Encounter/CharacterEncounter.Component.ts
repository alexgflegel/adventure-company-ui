import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Character, EncounterCharacter } from "../../../Models";
import { MatDialog } from "@angular/material/dialog";
import { EncounterCharacterDialogComponent } from "../../EncounterCharacter";

@Component({
	selector: "financial-character-encounter",
	templateUrl: "CharacterEncounter.Component.html"
})
export class CharacterEncounterComponent
{
	@Input() public character: Character;
	@Output() public navigationCallback: EventEmitter<any> = new EventEmitter();

	constructor(private dialog: MatDialog) { }

	public Load(event: Event, encounterCharacter: EncounterCharacter): void
	{
		event.preventDefault();

		this.navigationCallback.emit(encounterCharacter.encounter);
	}

	public Edit(encounterCharacter: EncounterCharacter): void
	{
		this.dialog.open(EncounterCharacterDialogComponent, {
			panelClass: "dialog-fill",
			data: { model: encounterCharacter, create: false }
		});
	}
}
