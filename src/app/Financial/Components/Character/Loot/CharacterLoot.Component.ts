﻿import { Component, Input } from "@angular/core";
import { Character, Transaction } from "../../../Models";

@Component({
	selector: "financial-character-loot",
	templateUrl: "CharacterLoot.Component.html",
	styleUrls: ["../../../../Framework/Styles/accordion.scss"]
})
export class CharacterLootComponent
{
	@Input() public character: Character;

	public Load(event: Event, transaction: Transaction): void
	{
		event.preventDefault();
	}
}
