import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Character, CharacterState } from "../../../Models/Character";
import { CharacterDialogComponent } from "./CharacterDialog.Component";
import { MatDialog } from "@angular/material/dialog";
import { Encounter } from "../../../Models/Encounter";

@Component({
	selector: "financial-character-detail",
	templateUrl: "CharacterDetail.Component.html",
	styles: [
		".mat-step-label.mat-step-label-active { flex-grow: 1; }"
	]
})
export class CharacterDetailComponent
{
	@Input() public character: Character;
	@Output() public navigationCallback: EventEmitter<any> = new EventEmitter();

	public characterStates: any = CharacterState;

	constructor(private dialog: MatDialog, ) { }

	public Load(event: Encounter): void
	{
		this.navigationCallback.emit(event);
	}

	public Edit(): void
	{
		this.dialog.open(CharacterDialogComponent, {
			panelClass: "dialog-fill",
			data: { model: this.character, create: false }
		});
	}
}
