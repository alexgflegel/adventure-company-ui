import { Component, Inject } from "@angular/core";
import { Character, CharacterState } from "../../../Models";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { IBaseDialog } from "../../../../Framework/Components/Dialog/IBaseDialog";
export interface CharacterDialogData
{
	model: Character;
	create: boolean;
}
@Component({
	selector: "financial-character-dialog",
	templateUrl: "CharacterDialog.Component.html",
	styles: [
		".mat-step-label.mat-step-label-active { flex-grow: 1; }"
	]
})
export class CharacterDialogComponent implements IBaseDialog
{
	public character: Character;
	public characterStates = CharacterState;

	constructor(public dialogRef: MatDialogRef<CharacterDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: CharacterDialogData)
	{
		this.character = Object.create(this.data.model);
	}

	public Abort(): void
	{
		this.dialogRef.close();
	}

	public Accept(): void
	{
		this.data.model.Update(this.character);
		this.dialogRef.close(this.data.model);
	}

	public Delete(): void
	{
		this.data.model.Delete();
		this.dialogRef.close();
	}
}
