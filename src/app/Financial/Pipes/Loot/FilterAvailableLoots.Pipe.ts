﻿import { Pipe, PipeTransform } from "@angular/core";

import { Encounter, Loot } from "../../../Financial/Models";
import { LootMode } from "../../Components/Loot";
import { FilterDeleted } from "..";
import { SumTransactionQuantity } from "../Transaction";

@Pipe({
	name: "FilterAvailableLoots", pure: false
})
export class FilterAvailableLoots implements PipeTransform
{
	/**
	 * returns a list of loot containing all loot with at least one remaining quantity
	 * @param encounters
	 * @param lootMode
	 */
	public transform(encounters: Encounter[], lootMode: LootMode): Loot[]
	{
		if (!encounters)
		{
			return [];
		}

		const filterDeleted = new FilterDeleted();

		let result: Loot[] = [];
		encounters.forEach(item => result = result.concat(filterDeleted.transform(item.loots) as Loot[]));

		// if the mode is available only then filter out any loot that have a positive remaining loot
		if (lootMode === "Available")
		{
			return result.filter(loot => new SumTransactionQuantity().transform(loot) > -(+loot.quantity || 0));
		}

		return result;
	}
}
