﻿import { Pipe, PipeTransform } from "@angular/core";
import { Loot } from "../../../Financial/Models";

@Pipe({
	name: "OrderLoot"
})
export class OrderLoot implements PipeTransform
{
	/**
	 * Orders loot alphabetically by description
	 * @param loots
	 */
	public transform(loots: Loot[]): Loot[]
	{
		if (loots)
		{
			loots.sort((a: Loot, b: Loot) =>
			{
				if (a.description < b.description)
				{
					return -1;
				}
				else
				{
					return 1;
				}
			});
			return loots;
		}
	}
}
