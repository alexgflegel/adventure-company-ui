﻿import { Pipe, PipeTransform } from "@angular/core";
import { Loot } from "../../Models";

@Pipe({
	name: "SumLootValue"
})
export class SumLootValue implements PipeTransform
{
	/**
	 * Calculates the value * quantity ensuring both are numbers
	 * @param loot
	 */
	public transform(loot: Loot): number
	{
		if (!loot)
		{
			return 0;
		}

		// force the consideration of numbers because as the values change the new values are considered strings
		return (+loot.value) * (+loot.quantity);
	}
}
