﻿import { Pipe, PipeTransform } from "@angular/core";
import { Character, Transaction } from "../..//Models";
import { FilterDeleted } from "..";

@Pipe({
	name: "FilterClaimedLoot"
})
export class FilterClaimedLoot implements PipeTransform
{
	/**
	 * Returns a list of transactions that contain a loot object
	 * @param character
	 */
	public transform(character: Character): Transaction[]
	{
		if (!character)
		{
			return [];
		}

		return (new FilterDeleted().transform(character.transactions) as Transaction[]).filter(item => item.loot);
	}
}
