﻿import { Pipe, PipeTransform } from "@angular/core";
import { Loot } from "../../../Financial/Models";
import { SumTransactionQuantity } from "../Transaction";

@Pipe({
	name: "SumRemainingLoot", pure: false
})
export class SumRemainingLoot implements PipeTransform
{
	/**
	 * Returns Quantity - quantity from loot transactions
	 * @param loot
	 */
	public transform(loot: Loot): number
	{
		if (!loot)
		{
			return 0;
		}

		return (+loot.quantity || 0) + new SumTransactionQuantity().transform(loot);
	}
}
