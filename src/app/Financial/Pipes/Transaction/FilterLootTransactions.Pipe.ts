﻿import { Pipe, PipeTransform } from "@angular/core";
import { Character, Transaction, TransactionMode } from "../../../Financial/Models";
import { FilterDeleted } from "..";

@Pipe({
	name: "FilterLootTransactions"
})
export class FilterLootTransactions implements PipeTransform
{
	/**
	 * Returns a list of transactions that are not deleted and matching the given mode
	 * @param items
	 * @param mode
	 */
	public transform(items: Transaction[], mode: TransactionMode, character: Character): Transaction[]
	{
		if (!items)
		{
			return [];
		}

		const vals = (new FilterDeleted().transform(items) as Transaction[]).filter(item => !!item.lootId === (mode === "Loot"));

		if (character)
		{
			return vals.filter(item => item.characterId === character.characterId);
		}
		else
		{
			return vals;
		}
	}
}
