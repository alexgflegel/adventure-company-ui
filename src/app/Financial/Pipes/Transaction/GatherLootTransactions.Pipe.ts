﻿import { Pipe, PipeTransform } from "@angular/core";
import { Loot, Transaction } from "../../../Financial/Models";
import { FilterDeleted } from "..";

@Pipe({
	name: "GatherLootTransactions"
})
export class GatherLootTransactions implements PipeTransform
{
	/**
	 * Returns a list of all transactions for all loot
	 * @param loots
	 */
	public transform(loots: Loot[]): Transaction[]
	{
		if (!loots)
		{
			return [];
		}

		const filterDeleted = new FilterDeleted();
		let result: Transaction[] = [];

		loots.forEach(item => result = result.concat(filterDeleted.transform(item.transactions) as Transaction[]));

		return result;
	}
}
