﻿import { Pipe, PipeTransform } from "@angular/core";
import { Loot, Transaction } from "../../../Financial/Models";
import { FilterDeleted } from "..";

@Pipe({
	name: "SumTransactionQuantity"
})
export class SumTransactionQuantity implements PipeTransform
{
	/**
	 * Returns a sum of the !deleted transaction.quantity
	 * @param loot
	 */
	public transform(loot: Loot): number
	{
		if (!loot)
		{
			return 0;
		}

		return new FilterDeleted().transform(loot.transactions)
			.reduce((total, transaction: Transaction) => total + transaction.quantity, 0);
	}
}
