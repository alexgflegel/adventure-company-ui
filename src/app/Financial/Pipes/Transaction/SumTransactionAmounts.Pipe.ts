﻿import { Pipe, PipeTransform } from "@angular/core";
import { Transaction } from "../../../Financial/Models";
import { FilterDeleted } from "..";

@Pipe({
	name: "SumTransactionAmounts"
})
export class SumTransactionAmounts implements PipeTransform
{
	/**
	 * Returns a sum of the amount
	 * @param items
	 */
	public transform(items: Transaction[]): number
	{
		if (!items)
		{
			return 0;
		}

		return new FilterDeleted().transform(items)
			.reduce((total, transaction: Transaction) => total + (+transaction.amount || 0), 0);
	}
}
