﻿import { Pipe, PipeTransform } from "@angular/core";
import { EncounterCharacter } from "../../Models";
import { SumEncounterShares } from "../Encounter/SumEncounterShares.Pipe";
import { SumEncounterTransactions } from "../Encounter/SumEncounterTransactions.Pipe";

@Pipe({
	name: "SumCharacterEncounter", pure: false
})
export class SumCharacterEncounter implements PipeTransform
{
	/**
	 * Returns the share amount for the specific character and encounter
	 * @param encounterCharacter
	 */
	public transform(encounterCharacter: EncounterCharacter): number
	{
		if (!encounterCharacter)
		{
			return 0;
		}
		/* Loot Value * Character Encounter Shares
		 *             /
		 *   Total Encounter Shares
		 */

		return (new SumEncounterTransactions().transform(encounterCharacter.encounter) * encounterCharacter.shares)
			/ new SumEncounterShares().transform(encounterCharacter.encounter);
	}
}
