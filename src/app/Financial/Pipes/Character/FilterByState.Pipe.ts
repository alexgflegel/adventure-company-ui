﻿import { Pipe, PipeTransform } from "@angular/core";
import { Character, CharacterState } from "../../../Financial/Models";
import { FilterDeleted } from "..";

@Pipe({
	name: "FilterByState"
})
export class FilterByState implements PipeTransform
{
	/**
	 * Filters out any character not matching the state
	 * @param characters
	 * @param characterState
	 */
	public transform(characters: Character[], characterState: CharacterState): Character[]
	{
		if (!characters)
		{
			return [];
		}

		// filter out anything deleted and not from the desired state
		return (new FilterDeleted().transform(characters) as Character[]).filter(item => item.status === characterState);
	}
}
