﻿import { Pipe, PipeTransform } from "@angular/core";
import { Character } from "../../../Financial/Models";
import { FilterLootTransactions, SumTransactionAmounts } from "../Transaction";
import { SumEncounterCharacter } from "../Encounter/SumEncounterCharacter.Pipe";

@Pipe({
	name: "CharacterTotal"
})
export class CharacterTotal implements PipeTransform
{
	/**
	 * Returns a total of the character encounter shares, personal transactions, and loot payments.
	 * @param character
	 */
	public transform(character: Character): number
	{
		if (!character)
		{
			return 0;
		}

		const transactionTransformer = new SumTransactionAmounts();
		const lootFilter = new FilterLootTransactions();

		// sum the character encounter shares, add the regular transactions, subtract the loot transactions
		return new SumEncounterCharacter().transform(character.encounterCharacters)
			+ transactionTransformer.transform(lootFilter.transform(character.transactions, "Character", undefined)) // sum the regular transactions as is
			- transactionTransformer.transform(lootFilter.transform(character.transactions, "Loot", undefined)); // subtract the sum of the loot transactions
	}
}
