﻿import { Character } from "../../../Financial/Models";
import { map, startWith } from "rxjs/operators";
import { NgModel } from "@angular/forms";
import { FilterActiveCharacters } from "./FilterActiveCharacters.Pipe";
import { Observable } from "rxjs";
import { Pipe, PipeTransform } from "@angular/core";
import { OrderCharacters } from "./OrderCharacters.Pipe";

@Pipe({
	name: "AutoCompleteCharacter"
})
export class AutoCompleteCharacter implements PipeTransform
{
	public transform(model: NgModel, characterList: Character[], preserveCharacter: Character): Observable<Character[]>
	{
		return model.valueChanges
			.pipe(
				startWith<string | Character>(preserveCharacter ? preserveCharacter.name : ""),
				// turn the value into a string if it isn't already
				map(value => value ? (typeof value === "string" ? value : (value.name || "")) : ""),
				// if there is a value filter the list, otherwise return the full list
				map(name => this.Filter(name, characterList, preserveCharacter))
			);
	}

	/**
	 * Filter the list of characters by Active state and name
	 * @param name A string to filter the character list by
	 * @param characterList List of characters to filter
	 * @param preserveCharacter A character is in the final list
	 */
	private Filter(name: string, characterList: Character[], preserveCharacter: Character): Character[]
	{
		const list = new OrderCharacters().transform(new FilterActiveCharacters().transform(characterList, preserveCharacter));

		if (name)
		{
			return list.filter(character => !character.name.toLowerCase().indexOf(name.toLowerCase()));
		}
		else
		{
			return list;
		}
	}
}
