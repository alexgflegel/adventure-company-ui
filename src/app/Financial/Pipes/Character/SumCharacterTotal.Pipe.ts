﻿import { Pipe, PipeTransform } from "@angular/core";
import { Character } from "../../../Financial/Models";
import { FilterDeleted } from "..";
import { CharacterTotal } from "./CharacterTotal.Pipe";

@Pipe({
	name: "SumCharacterTotal"
})
export class SumCharacterTotal implements PipeTransform
{
	/**
	 * Returns a sum of each character total
	 * @param characters
	 */
	public transform(characters: Character[]): number
	{
		if (!characters)
		{
			return 0;
		}

		return new FilterDeleted().transform(characters)
			.reduce((total, character: Character) => total + new CharacterTotal().transform(character), 0);
	}
}
