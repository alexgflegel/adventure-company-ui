﻿import { Pipe, PipeTransform } from "@angular/core";
import { Character, CharacterState } from "../../../Financial/Models";
import { FilterDeleted } from "..";

@Pipe({
	name: "FilterActiveCharacters"
})
export class FilterActiveCharacters implements PipeTransform
{
	/**
	 * Filters deleted characters and any character !Active, and includes the the current character (optional)
	 * @param characters
	 * @param character
	 */
	public transform(characters: Character[], character?: Character): Character[]
	{
		if (!characters)
		{
			return [];
		}

		return (new FilterDeleted().transform(characters) as Character[]).filter(item => item.status === CharacterState.Active
			|| item.characterId === (character ? character.characterId : 0));
	}
}
