﻿import { Pipe, PipeTransform } from "@angular/core";
import { Character, CharacterState } from "../../../Financial/Models";

@Pipe({
	name: "OrderCharacters"
})
export class OrderCharacters implements PipeTransform
{
	/**
	 * Orders the characters alphabetically. If a state is given filter by state.
	 * @param characters
	 * @param characterState
	 */
	public transform(characters: Character[], characterState?: CharacterState): Character[]
	{
		if (characters)
		{
			let results: Character[] = characters;

			if (characterState)
			{
				results = characters.filter(item => item.status === characterState);
			}

			results.sort((a: Character, b: Character) =>
			{
				if (a.name < b.name)
				{
					return -1;
				}
				else
				{
					return 1;
				}
			});

			return results;
		}
	}
}
