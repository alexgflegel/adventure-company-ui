﻿import { Pipe, PipeTransform } from "@angular/core";
import { Character, CharacterState, Encounter, EncounterCharacter } from "../../../Financial/Models";
import { FilterDeleted } from "..";

@Pipe({
	name: "FilterAvailableCharacters"
})
export class FilterAvailableCharacters implements PipeTransform
{
	/**
	 * Filters out any character that is dead or deleted as well as those already assigned to the encounter
	 * @param characters
	 * @param encounter
	 */
	public transform(characters: Character[], encounter: Encounter): Character[]
	{
		if (!characters || !encounter)
		{
			return [];
		}

		const filterDeleted = new FilterDeleted();

		// run a find command to get an existing character if the character is not active
		return (filterDeleted.transform(characters) as Character[]).filter(item =>
			!(filterDeleted.transform(encounter.encounterCharacters) as EncounterCharacter[]).find(existing => existing.characterId === item.characterId)
			&& item.status !== CharacterState.Dead);
	}
}
