﻿import { Pipe, PipeTransform } from "@angular/core";
import { ModelState } from "../../Financial/Models/ModelState";

@Pipe({
	name: "FilterDeleted"
})
export class FilterDeleted implements PipeTransform
{
	/**
	 * Filters out all deleted items
	 * @param items
	 */
	public transform(items: ModelState[]): ModelState[]
	{
		if (!items)
		{
			return [];
		}

		return items.filter(item => !item.IsDeleted);
	}
}
