﻿import { Pipe, PipeTransform } from "@angular/core";
import { Encounter, Loot } from "../../Models";
import { FilterDeleted } from "..";
import { SumTransactionAmounts } from "../Transaction";

@Pipe({
	name: "SumEncounterTransactions"
})
export class SumEncounterTransactions implements PipeTransform
{
	/**
	 * Sums the amount for all transactions and loot transactions on an encounter
	 * @param encounter
	 */
	public transform(encounter: Encounter): number
	{
		if (!encounter)
		{
			return 0;
		}

		const sumAmount = new SumTransactionAmounts();

		return (new FilterDeleted().transform(encounter.loots)
			.reduce((lootTotal, loot: Loot) => lootTotal + sumAmount.transform(loot.transactions), 0)
			// sum encounter transactions
			+ sumAmount.transform(encounter.transactions));
	}
}
