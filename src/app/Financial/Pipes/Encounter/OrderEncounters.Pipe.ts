﻿import { Pipe, PipeTransform } from "@angular/core";
import { Encounter } from "../../../Financial/Models";

@Pipe({
	// The @Pipe decorator takes an object with a name property whose value is the pipe name that we'll use within a template expression.
	// It must be a valid JavaScript identifier. Our pipe's name is orderby.
	name: "OrderEncounters"
})
export class OrderEncounters implements PipeTransform
{
	/**
	 * Orders encounters alphabetically by description
	 * @param encounters
	 */
	public transform(encounters: Encounter[]): Encounter[]
	{
		if (encounters)
		{
			encounters.sort((a: Encounter, b: Encounter) =>
			{
				if (a.description < b.description)
				{
					return -1;
				}
				else
				{
					return 1;
				}
			});
			return encounters;
		}
	}
}
