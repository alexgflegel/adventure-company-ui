﻿import { Pipe, PipeTransform } from "@angular/core";
import { EncounterCharacter } from "../../../Financial/Models";

@Pipe({
	name: "OrderEncounterCharacters"
})
export class OrderEncounterCharacters implements PipeTransform
{
	/**
	 * Orders encounterCharacters alphabetically by character with empty records last
	 * @param encounterCharacters
	 */
	public transform(encounterCharacters: EncounterCharacter[]): EncounterCharacter[]
	{
		if (encounterCharacters)
		{
			encounterCharacters.sort((a: EncounterCharacter, b: EncounterCharacter) =>
			{
				if (a.character && b.character)
				{
					if (a.character.name < b.character.name)
					{
						return -1;
					}
					else
					{
						return 1;
					}
				}
				else if (a.character)
				{
					return -1;
				}
				else if (b.character)
				{
					return 1;
				}
				else
				{
					return 0;
				}
			});

			return encounterCharacters;
		}
	}
}
