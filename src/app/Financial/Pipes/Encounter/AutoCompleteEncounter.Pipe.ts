﻿import { map, startWith } from "rxjs/operators";
import { NgModel } from "@angular/forms";
import { Observable } from "rxjs";
import { Pipe, PipeTransform } from "@angular/core";
import { Encounter, EncounterCharacter } from "../../Models";
import { FilterAvailableEncounters } from "./FilterAvailableEncounters.Pipe";
import { OrderEncounters } from "./OrderEncounters.Pipe";

@Pipe({
	name: "AutoCompleteEncounter"
})
export class AutoCompleteEncounter implements PipeTransform
{
	public transform(model: NgModel, encounterCharacter: EncounterCharacter): Observable<Encounter[]>
	{
		return model.valueChanges
			.pipe(
				startWith<string | Encounter>(encounterCharacter.encounter ? encounterCharacter.encounter.description : ""),
				// turn the value into a string if it isn't already
				map(value => value ? (typeof value === "string" ? value : (value.description || "")) : ""),
				// if there is a value filter the list, otherwise return the full list
				map(name => this.Filter(name, encounterCharacter))
			);
	}

	/**
	 * Filter the list of characters by Active state and name
	 * @param name A string to filter the character list by
	 * @param encounter List of characters to filter
	 */
	private Filter(name: string, encounterCharacter: EncounterCharacter): Encounter[]
	{
		let shortList = new FilterAvailableEncounters().transform(encounterCharacter.encounter.company.encounters, encounterCharacter.character);

		// ensure the current character is saved
		if (encounterCharacter.encounterId)
		{
			shortList.push(encounterCharacter.encounter.company.GetEncounter(encounterCharacter.encounterId));
		}

		shortList = new OrderEncounters().transform(shortList);

		if (name)
		{
			return shortList.filter(item => !item.description.toLowerCase().indexOf(name.toLowerCase()));
		}
		else
		{
			return shortList;
		}
	}
}
