﻿import { Character, EncounterCharacter } from "../../../Financial/Models";
import { map, startWith } from "rxjs/operators";
import { NgModel } from "@angular/forms";
import { Observable } from "rxjs";
import { Pipe, PipeTransform } from "@angular/core";
import { FilterAvailableCharacters, OrderCharacters } from "../Character";

@Pipe({
	name: "AutoCompleteEncounterCharacter"
})
export class AutoCompleteEncounterCharacter implements PipeTransform
{
	/**
	 * Returns a list of available characters based on live input
	 * @param character
	 * @param encounterCharacter
	 */
	public transform(character: NgModel, encounterCharacter: EncounterCharacter): Observable<Character[]>
	{
		return character.valueChanges
			.pipe(
				startWith<string | Character>(encounterCharacter.character ? encounterCharacter.character.name : ""),
				// turn the value into a string if it isn't already
				map(value => value ? (typeof value === "string" ? value : (value.name || "")) : ""),
				// if there is a value filter the list, otherwise return the full list
				map(name => this.Filter(name, encounterCharacter))
			);
	}

	private Filter(name: string, encounterCharacter: EncounterCharacter): Character[]
	{
		let shortList = new FilterAvailableCharacters().transform(encounterCharacter.encounter.company.characters, encounterCharacter.encounter);

		// ensure the current character is saved
		if (encounterCharacter.characterId)
		{
			shortList.push(encounterCharacter.encounter.company.GetCharacter(encounterCharacter.characterId));
		}

		shortList = new OrderCharacters().transform(shortList);

		if (name)
		{
			return shortList.filter(character => !character.name.toLowerCase().indexOf(name.toLowerCase()));
		}
		else
		{
			return shortList;
		}
	}
}
