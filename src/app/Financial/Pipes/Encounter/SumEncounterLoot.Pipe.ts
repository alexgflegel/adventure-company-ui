﻿import { Pipe, PipeTransform } from "@angular/core";
import { Loot } from "../../Models";
import { FilterDeleted } from "..";
import { SumLootValue } from "../Loot";

@Pipe({
	name: "SumEncounterLoot"
})
export class SumEncounterLoot implements PipeTransform
{
	/**
	 * Sums the total value of loot * quantity
	 * @param loots
	 */
	public transform(loots: Loot[]): number
	{
		if (!loots)
		{
			return 0;
		}

		return new FilterDeleted().transform(loots).reduce((sum, item: Loot) => sum + new SumLootValue().transform(item), 0);
	}
}
