﻿import { Pipe, PipeTransform } from "@angular/core";
import { Character, Encounter, EncounterCharacter } from "../../Models";
import { FilterDeleted } from "..";

@Pipe({
	name: "FilterAvailableEncounters"
})
export class FilterAvailableEncounters implements PipeTransform
{
	/**
	 * Filters out any character that is dead or deleted as well as those already assigned to the encounter
	 * @param characters
	 * @param encounter
	 */
	public transform(encounters: Encounter[], character: Character): Encounter[]
	{
		if (!encounters || !character)
		{
			return [];
		}

		const filterDeleted = new FilterDeleted();

		// run a find command to get an existing character if the character is not active
		return (filterDeleted.transform(encounters) as Encounter[]).filter(item =>
			!(filterDeleted.transform(character.encounterCharacters) as EncounterCharacter[]).find(existing => existing.encounterId === item.encounterId));
	}
}
