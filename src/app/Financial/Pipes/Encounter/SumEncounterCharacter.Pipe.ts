﻿import { Pipe, PipeTransform } from "@angular/core";
import { EncounterCharacter } from "../../../Financial/Models";
import { FilterDeleted } from "..";
import { SumCharacterEncounter } from "../Character/SumCharacterEncounter.Pipe";

@Pipe({
	name: "SumEncounterCharacter"
})
export class SumEncounterCharacter implements PipeTransform
{
	/**
	 * Sums each character encounter total
	 * @param encounterCharacters
	 */
	public transform(encounterCharacters: EncounterCharacter[]): number
	{
		if (!encounterCharacters)
		{
			return 0;
		}
		/*          Loot Value * Character Encounter Shares
		 *          ---------------------------------------
		 *                   Total Encounter Shares
		 */

		return new FilterDeleted().transform(encounterCharacters)
			.reduce((total, encounterCharacter: EncounterCharacter) => total + new SumCharacterEncounter().transform(encounterCharacter), 0);
	}
}
