﻿import { Pipe, PipeTransform } from "@angular/core";
import { Encounter, EncounterCharacter } from "../../../Financial/Models";
import { FilterDeleted } from "..";

@Pipe({
	name: "SumEncounterShares", pure: false
})
export class SumEncounterShares implements PipeTransform
{
	/**
	 * Sums all shares for an encounter
	 * @param encounter
	 */
	public transform(encounter: Encounter): number
	{
		if (!encounter)
		{
			return 0;
		}

		return new FilterDeleted().transform(encounter.encounterCharacters)
			.reduce((total, encounterCharacter: EncounterCharacter) => total + encounterCharacter.shares, 0);
	}
}
