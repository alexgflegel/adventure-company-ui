﻿import { Character, Encounter, Loot } from ".";
import { EntityState, ModelState } from "./ModelState";

export type TransactionMode = "Encounter" | "Loot" | "Character";

export class Transaction extends ModelState
{
	public transactionId: number;
	public encounterId: number;
	public lootId: number;
	public characterId: number;

	public amount: number;
	public quantity: number;
	public value: number;
	public timestamp: Date;

	public name: string;
	public description: string;

	// terminated connections
	public character: Character;
	public encounter: Encounter;
	public loot: Loot;

	private static index = 0;

	constructor(encounter?: Encounter, loot?: Loot, character?: Character, transaction?: Transaction)
	{
		super();

		this.quantity = 0;
		this.amount = 0;

		if (encounter)
		{
			this.encounter = encounter;
			this.encounterId = encounter.encounterId;
		}

		if (loot)
		{
			this.loot = loot;
			this.lootId = loot.lootId;
		}

		if (character)
		{
			this.character = character;
			this.characterId = character.characterId;

			// loot transactions have been pruned from the character transaction list during the prepare phase
			this.character.transactions.push(this);
		}

		if (transaction)
		{
			this.modelState = transaction.modelState || EntityState.Unchanged;
			this.transactionId = transaction.transactionId;

			if (!character && transaction.characterId)
			{
				this.characterId = transaction.characterId;

				if (this.loot)
				{
					this.character = this.loot.encounter.company.GetCharacter(this.characterId);
				}
				else
				{
					this.character = this.encounter.company.GetCharacter(this.characterId);
				}

				this.character.transactions.push(this);
			}

			this.amount = transaction.amount;
			this.quantity = transaction.quantity;
			this.value = transaction.value;
			this.timestamp = transaction.timestamp;

			this.name = transaction.name;
			this.description = transaction.description;
		}
		else
		{
			this.transactionId = --Transaction.index;
		}
	}

	public PrepareSave(): void
	{
		super.PrepareSave();

		this.character = undefined;
		this.encounter = undefined;
		this.loot = undefined;
	}

	/**
	 * Triggers updating a character and handling pruning and pusing transactions.
	 */
	public UpdateCharacter(): void
	{
		if (this.characterId && this.characterId.toString() === "")
		{
			this.characterId = undefined;
		}

		// check to see if this is a character transaction
		if (this.characterId)
		{
			// depending on the transaction type the company source might be different
			const company = (this.encounter || (this.loot && this.loot.encounter) || this.character).company;

			if (!this.character || typeof this.character === "string")
			{
				// the character has been unset, clear the character
				company.GetCharacter(this.characterId).PruneTransaction(this);
				this.characterId = undefined;
			}
			else if (this.characterId !== this.character.characterId)
			{
				// if the character has changed the transaction needs to be moved.
				// prune the transaction from the old character
				company.GetCharacter(this.characterId).PruneTransaction(this);

				if (this.character)
				{
					this.character.transactions.push(this);
					this.characterId = this.character.characterId;
				}
			}
		}
		else if (this.character)
		{
			this.characterId = this.character.characterId;
			this.character.transactions.push(this);
		}
	}

	/**
	 * Dirties the object and processes changing the transaction from one character to another
	 */
	public Dirty(): void
	{
		if (this.character)
		{
			this.character.transactions = Object.assign([], this.character.transactions);
		}

		if (this.encounter)
		{
			this.encounter.transactions = Object.assign([], this.encounter.transactions);
		}

		if (this.loot)
		{
			this.loot.transactions = Object.assign([], this.loot.transactions);
		}

		super.Dirty();
	}

	public Update(model: Transaction): void
	{
		this.encounterId = model.encounterId;
		this.lootId = model.lootId;
		this.characterId = model.characterId;

		this.quantity = model.quantity;
		this.amount = model.amount;
		this.value = model.value;
		this.timestamp = model.timestamp;

		this.description = model.description;
		this.name = model.name;

		this.character = model.character;
		this.encounter = model.encounter;
		this.loot = model.loot;

		super.Update(model);
	}
}
