﻿import { Character, Encounter } from "./";
import { EntityState, ModelState } from "./ModelState";

export class EncounterCharacter extends ModelState
{
	public characterId: number;
	public encounterId: number;
	public shares: number;

	// terminated connections
	public character: Character;
	public encounter: Encounter;

	constructor(encounter?: Encounter, character?: Character, encounterCharacter?: EncounterCharacter)
	{
		super();
		this.shares = 1;

		if (encounter)
		{
			this.encounterId = encounter.encounterId;
			this.encounter = encounter;

			if (!character && encounterCharacter)
			{
				this.characterId = encounterCharacter.characterId;
				this.character = this.encounter.company.GetCharacter(this.characterId);
				this.character.encounterCharacters.push(this);
			}
		}

		if (character)
		{
			this.characterId = character.characterId;
			this.character = character;
			this.character.encounterCharacters.push(this);
		}

		if (encounterCharacter)
		{
			this.modelState = encounterCharacter.modelState || EntityState.Unchanged;
			this.shares = encounterCharacter.shares;
		}
	}

	public PrepareSave(): void
	{
		super.PrepareSave();

		this.character = undefined;
		this.encounter = undefined;
	}

	/**
	 * Updates the character connections
	 */
	public UpdateCharacter(): void
	{
		if (this.characterId && this.characterId.toString() === "")
		{
			this.characterId = undefined;
		}

		if (this.characterId)
		{
			if (!this.character || typeof this.character === "string")
			{
				// the character has been unset, clear the character
				this.encounter.company.GetCharacter(this.characterId).PruneEncounterCharacter(this);
				this.characterId = undefined;
			}
			else if (this.characterId !== this.character.characterId)
			{
				// if the character has changed the encounterCharacter needs to be moved.
				// prune the transaction from the old character
				this.encounter.company.GetCharacter(this.characterId).PruneEncounterCharacter(this);

				if (this.character)
				{
					this.characterId = this.character.characterId;
					this.character.encounterCharacters.push(this);
				}
			}
		}
		else if (this.character)
		{
			this.characterId = this.character.characterId;
			this.character.encounterCharacters.push(this);
		}
	}

	/**
	 * Updates the encounter connections
	 */
	public UpdateEncounter(): void
	{
		if (this.encounterId && this.encounterId.toString() === "")
		{
			this.encounterId = undefined;
		}

		if (this.encounterId)
		{
			if (!this.encounter || typeof this.encounter === "string")
			{
				// the encounter has been unset, clear the encounter
				this.encounter.company.GetEncounter(this.encounterId).PruneEncounterCharacter(this);
				this.encounterId = undefined;
			}
			else if (this.encounterId !== this.encounter.encounterId)
			{
				// if the character has changed the encounterCharacter needs to be moved.
				// prune the transaction from the old character
				this.encounter.company.GetEncounter(this.encounterId).PruneEncounterCharacter(this);

				if (this.encounter)
				{
					this.encounterId = this.encounter.encounterId;
					this.encounter.encounterCharacters.push(this);
				}
			}
		}
		else if (this.encounter)
		{
			this.encounterId = this.encounter.encounterId;
			this.encounter.encounterCharacters.push(this);
		}
	}

	/**
	 * Dirties the object and processes changing the transaction from one character to another
	 */
	public Dirty(): void
	{
		super.Dirty();
		this.encounter.encounterCharacters = Object.assign([], this.encounter.encounterCharacters);
		this.character.encounterCharacters = Object.assign([], this.character.encounterCharacters);
	}

	public Update(model: EncounterCharacter): void
	{
		this.encounterId = model.encounterId;
		this.characterId = model.characterId;
		this.shares = model.shares;

		this.character = model.character;
		this.encounter = model.encounter;

		this.encounter = Object.assign({}, this.encounter);
		this.encounter.encounterCharacters = Object.assign([], this.encounter.encounterCharacters);
		this.encounter.encounterCharacters.forEach(item => item = Object.assign({}, item));

		super.Update(model);
	}
}
