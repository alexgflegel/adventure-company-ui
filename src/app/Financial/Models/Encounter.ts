﻿import { Company } from "./Company";
import { EntityState, ModelState } from "./ModelState";
import { Loot } from "./Loot";
import { Transaction } from "./Transaction";
import { EncounterCharacter } from "./EncounterCharacter";

export class Encounter extends ModelState
{
	public encounterId: number;
	public description: string;
	public companyId: number;
	public timeStamp: Date;

	public loots: Loot[];
	public transactions: Transaction[];
	public encounterCharacters: EncounterCharacter[];

	// terminated connections
	public company: Company;

	private static index = 0;

	constructor(company: Company, encounter?: Encounter)
	{
		super();
		this.loots = [];
		this.transactions = [];
		this.encounterCharacters = [];

		if (company)
		{
			this.company = company;
			this.companyId = company.companyId;
		}

		if (encounter)
		{
			this.modelState = encounter.modelState || EntityState.Unchanged;

			this.encounterId = encounter.encounterId;
			this.description = encounter.description;
			this.timeStamp = encounter.timeStamp;

			this.loots = encounter.loots.map(loot => new Loot(this, loot));
			this.transactions = encounter.transactions.map(transaction => new Transaction(this, undefined, undefined, transaction));
			this.encounterCharacters = encounter.encounterCharacters.map(encounterCharacter => new EncounterCharacter(this, undefined, encounterCharacter));
		}
		else
		{
			this.encounterId = --Encounter.index;
		}
	}

	/**
	 * Override the base delete to cascade to child objects
	 */
	public Delete(): void
	{
		// delete the transactions
		this.transactions.forEach(transaction => transaction.Delete());

		// delete the encounter characters
		this.encounterCharacters.forEach(encounterCharacter => encounterCharacter.Delete());

		// delete the loots
		this.loots.forEach(loot => loot.Delete());

		super.Delete();
	}

	public Dirty(): void
	{
		super.Dirty();
		this.company.encounters = Object.assign([], this.company.encounters);
	}

	public PrepareSave(): void
	{
		super.PrepareSave();

		this.company = undefined;

		// disconnect detached objects, these objects were created and deleted since the last save
		this.encounterCharacters = this.encounterCharacters.filter(item => !item.IsDetached);
		this.encounterCharacters.forEach(item => item.PrepareSave());

		this.transactions = this.transactions.filter(item => !item.IsDetached);
		this.transactions.forEach(item => item.PrepareSave());

		this.loots = this.loots.filter(item => !item.IsDetached);
		this.loots.forEach(item => item.PrepareSave());
	}

	/**
	 * Removes the specified encounterCharacter from the encounterCharacters list
	 * @param encounterCharacter EncounterCharacter
	 */
	public PruneEncounterCharacter(encounterCharacter: EncounterCharacter): void
	{
		const encounterIndex = this.GetEncounterCharacter(encounterCharacter.characterId, encounterCharacter.encounterId);

		this.encounterCharacters.splice(this.encounterCharacters.indexOf(encounterIndex), 1);
		this.encounterCharacters = Object.assign([], this.encounterCharacters);
	}

	/**
	 * Returns a specific encounter character
	 * @param characterId
	 * @param encounterId
	 */
	public GetEncounterCharacter(characterId: number, encounterId: number): EncounterCharacter
	{
		if (!characterId || !encounterId)
		{
			return undefined;
		}

		return this.encounterCharacters.find(f => f.characterId === characterId && f.encounterId === encounterId);
	}

	public Update(model: Encounter): void
	{
		this.description = model.description;

		super.Update(model);
	}
}

export const EncounterDisplay = (encounter?: Encounter): string | undefined => encounter ? encounter.description : undefined;
