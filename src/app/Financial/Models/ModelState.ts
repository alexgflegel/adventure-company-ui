﻿/**
 *  A copy of the EntityState from EntityFrameworkCore
 */
export enum EntityState
{
	//
	// Summary:
	//     The entity is not being tracked by the context.
	Detached = 0,
	//
	// Summary:
	//     The entity is being tracked by the context and exists in the database. Its property
	//     values have not changed from the values in the database.
	Unchanged = 1,
	//
	// Summary:
	//     The entity is being tracked by the context and exists in the database. It has
	//     been marked for deletion from the database.
	Deleted = 2,
	//
	// Summary:
	//     The entity is being tracked by the context and exists in the database. Some or
	//     all of its property values have been modified.
	Modified = 3,
	//
	// Summary:
	//     The entity is being tracked by the context but does not yet exist in the database.
	Added = 4
}

export enum PermissionLevel
{
	Owner = 0,
	Read = 1,
	Write = 2
}

export type StateMachineActions = "Dirty" | "Delete" | "Undelete";

export class ModelState
{
	protected modelState: EntityState;

	constructor()
	{
		// default the modelstate to added
		this.modelState = EntityState.Added;
	}

	/**
	 * Detaches upsteam objects, removes any "detached" items from child lists
	 */
	public PrepareSave(): void { }

	/**
	 * Sets the model state and rebuilds validation
	 */
	public Dirty(): void
	{
		this.StateMachine("Dirty");
	}

	/**
	 * Soft deletes an object and manages deleting newly added items
	 */
	public Delete(): void
	{
		this.StateMachine("Delete");
		this.Dirty();
	}

	/**
	 * Modifies the modelstate to a correct non deleted object
	 */
	public Undelete(): void
	{
		this.StateMachine("Undelete");
	}

	/**
	 * Checks the modelstate for deleted flags
	 */
	public get IsDeleted(): boolean
	{
		// Deleted marks the object for database deletion, Detached marks objects to be pruned before submitting to the server
		// either state is to be treated as deleted, the detached state is caught because it is 0
		return !this.modelState || this.modelState === EntityState.Deleted;
	}

	/**
	 * Checks the modelstate for deleted flags
	 */
	public get IsDetached(): boolean
	{
		return !this.modelState;
	}

	/**
	 * Updates a model from a dialog and reconnecting child objects
	 * @param model a model to update each class
	 */
	public Update(model: ModelState): void
	{
		this.Dirty();
	}

	/**
	 * Handles updating the entity state to help the api work
	 * @param action
	 */
	private StateMachine(action: StateMachineActions): void
	{
		switch (this.modelState)
		{
			case EntityState.Detached:
				switch (action)
				{
					case "Delete":
					case "Dirty":
						break;
					case "Undelete":
						this.modelState = EntityState.Added;
						break;
				}
				break;
			case EntityState.Unchanged:
				switch (action)
				{
					case "Delete":
						this.modelState = EntityState.Deleted;
						break;
					case "Dirty":
					case "Undelete":
						this.modelState = EntityState.Modified;
						break;
				}
				break;
			case EntityState.Deleted:
				switch (action)
				{
					case "Delete":
					case "Dirty":
						break;
					case "Undelete":
						this.modelState = EntityState.Modified;
						break;
				}
				break;
			case EntityState.Modified:
				switch (action)
				{
					case "Delete":
						this.modelState = EntityState.Deleted;
						break;
					case "Dirty":
					case "Undelete":
						break;
				}
				break;
			case EntityState.Added:
				switch (action)
				{
					case "Delete":
						this.modelState = EntityState.Detached;
						break;
					case "Dirty":
					case "Undelete":
						break;
				}
				break;
			default:
				this.modelState = EntityState.Unchanged;
				break;
		}
	}
}
