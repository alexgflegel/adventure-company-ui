import { ModelState, PermissionLevel, EntityState } from "./ModelState";
import { Company } from "./Company";

export class CompanyPermission extends ModelState
{
	public companyId: number;
	public userId: string;
	// email: string;
	public permission: PermissionLevel;

	// terminated connections
	public company: Company;

	constructor(company: Company, companyPermission?: CompanyPermission)
	{
		super();

		if (company)
		{
			this.company = company;
			this.companyId = company.companyId;
		}

		if (companyPermission)
		{
			this.modelState = companyPermission.modelState || EntityState.Unchanged;

			this.userId = companyPermission.userId;
			this.permission = companyPermission.permission;
		}
	}

	public PrepareSave(): void
	{
		this.company = undefined;
	}
}
