﻿import { Company } from "./Company";
import { EntityState, ModelState } from "./ModelState";
import { Transaction } from "./Transaction";
import { EncounterCharacter } from "./EncounterCharacter";

export class Character extends ModelState
{
	public characterId: number;
	public name: string;
	public description: string;
	public userId: string;
	public companyId: number;
	public status: CharacterState;

	public transactions: Transaction[];

	// terminated connections
	public company: Company;
	public encounterCharacters: EncounterCharacter[];

	private static index = 0;

	constructor(company: Company, character?: Character)
	{
		super();
		this.transactions = [];
		this.encounterCharacters = [];

		// initialize the parent object
		if (company)
		{
			this.company = company;
			this.companyId = company.companyId;
		}

		// if this constructor is copying a character (during page load), replicate all fields manually
		if (character)
		{
			this.modelState = character.modelState || EntityState.Unchanged;

			this.characterId = character.characterId;
			this.name = character.name;
			this.description = character.description;
			this.userId = character.userId;
			this.status = character.status;

			this.transactions = character.transactions;
			this.PruneTransactions(true);
			this.transactions = this.transactions.map(transaction => new Transaction(undefined, undefined, this, transaction));
		}
		else
		{
			this.characterId = --Character.index;
		}
	}

	public Dirty(): void
	{
		super.Dirty();
		this.company.characters = Object.assign([], this.company.characters);
	}

	public Delete(): void
	{
		// delete the transactions
		this.transactions.forEach(transaction => transaction.Delete());

		// delete the encounter characters
		this.encounterCharacters.forEach(encounterCharacter => encounterCharacter.Delete());

		super.Delete();
	}

	public PrepareSave(): void
	{
		super.PrepareSave();

		this.company = undefined;
		this.encounterCharacters = undefined;

		this.PruneTransactions();

		this.transactions = this.transactions.filter(item => !item.IsDetached);
		this.transactions.forEach(item => item.PrepareSave());
	}

	public Update(model: Character): void
	{
		this.name = model.name;
		this.description = model.description;
		this.status = model.status;

		super.Update(model);
	}

	/**
	 * Removes the specified transaction from the transactions list
	 * @param transaction Transaction
	 */
	public PruneTransaction(transaction: Transaction): void
	{
		this.transactions.splice(this.transactions.indexOf(transaction), 1);
	}

	/**
	 * Removes the specified encounterCharacter from the encounterCharacters list
	 * @param encounterCharacter EncounterCharacter
	 */
	public PruneEncounterCharacter(encounterCharacter: EncounterCharacter): void
	{
		this.encounterCharacters.splice(this.encounterCharacters.indexOf(encounterCharacter), 1);
		this.encounterCharacters = Object.assign([], this.encounterCharacters);
	}

	/**
	 * Finds all transactions needing to be pruned and prunes them
	 * @param skipState Enables skipping the IsDetached function which can be uninitialized
	 */
	private PruneTransactions(skipState: boolean = false): void
	{
		// find and prune any loot transactions.  loot transactions are handled by the loot object
		const lootTransactions: Transaction[] = this.transactions.filter(item => item.lootId && (skipState || !item.IsDetached));

		lootTransactions.map(lootTransaction => this.PruneTransaction(lootTransaction));
	}
}

export const CharacterDisplay = (character?: Character): string | undefined => character ? character.name : undefined;

export enum CharacterState
{
	Active = 0,
	Inactive = 1,
	Dead = 2
}
