﻿import { Encounter } from "./Encounter";
import { EntityState, ModelState } from "./ModelState";
import { Transaction } from "./Transaction";

export class Loot extends ModelState
{
	public lootId: number;
	public description: string;
	public encounterId: number;
	public value: number;
	public quantity: number;

	public transactions: Transaction[];

	// terminated connections
	public encounter: Encounter;

	private static index = 0;

	constructor(encounter: Encounter, loot?: Loot)
	{
		super();
		this.transactions = [];

		this.value = 0;
		this.quantity = 1;
		this.description = "Unknown Item";

		if (encounter)
		{
			this.encounterId = encounter.encounterId;
			this.encounter = encounter;
		}

		if (loot)
		{
			this.modelState = loot.modelState || EntityState.Unchanged;

			this.lootId = loot.lootId;
			this.description = loot.description;
			this.value = loot.value;
			this.quantity = loot.quantity;

			this.transactions = loot.transactions.map(transaction => new Transaction(undefined, this, undefined, transaction));
		}
		else
		{
			this.lootId = --Loot.index;
		}
	}

	public Dirty(): void
	{
		super.Dirty();

		this.encounter.loots = Object.assign([], this.encounter.loots);
	}

	public Delete(): void
	{
		this.transactions.forEach(transaction => transaction.Delete());

		super.Delete();
	}

	public PrepareSave(): void
	{
		super.PrepareSave();

		this.encounter = undefined;

		this.transactions = this.transactions.filter(item => !item.IsDetached);
		this.transactions.forEach(item => item.PrepareSave());
	}

	public Update(model: Loot): void
	{
		this.encounterId = model.encounterId;
		this.quantity = model.quantity;
		this.value = model.value;
		this.description = model.description;

		this.encounter = model.encounter;

		super.Update(model);
	}
}
