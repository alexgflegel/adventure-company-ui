﻿import { Character } from "./Character";
import { EntityState, ModelState, PermissionLevel } from "./ModelState";
import { Encounter } from "./Encounter";

export interface CompanyIndex
{
	myCompanies: Company[];
	sharedCompanies: Company[];
}

export class CompanyPermission extends ModelState
{
	public companyId: number;
	public userId: string;
	// email: string;
	public permission: PermissionLevel;

	// terminated connections
	public company: Company;

	constructor(company: Company, companyPermission?: CompanyPermission)
	{
		super();

		if (company)
		{
			this.company = company;
			this.companyId = company.companyId;
		}

		if (companyPermission)
		{
			this.modelState = companyPermission.modelState || EntityState.Unchanged;

			this.userId = companyPermission.userId;
			this.permission = companyPermission.permission;
		}
	}

	public PrepareSave(): void
	{
		this.company = undefined;
	}
}

export class Company extends ModelState
{
	public companyId: number;
	public name: string;
	public description: string;

	public characters: Character[];
	public companyPermissions: CompanyPermission[];
	public encounters: Encounter[];

	constructor(company?: Company)
	{
		super();
		this.characters = [];
		this.encounters = [];
		this.companyPermissions = [];

		if (company)
		{
			this.modelState = company.modelState || EntityState.Unchanged;
			this.companyId = company.companyId;
			this.name = company.name;
			this.description = company.description;

			this.characters = company.characters.map(character => new Character(this, character));
			this.encounters = company.encounters.map(encounter => new Encounter(this, encounter));
			this.companyPermissions = company.companyPermissions.map(companyPermission => new CompanyPermission(this, companyPermission));
		}
	}

	public Delete(): void
	{
		this.characters.forEach(character => character.Delete());
		this.encounters.forEach(encounter => encounter.Delete());

		super.Delete();
	}

	public PrepareSave(): void
	{
		super.PrepareSave();

		this.characters = this.characters.filter(item => !item.IsDetached);
		this.characters.forEach(item => item.PrepareSave());

		this.companyPermissions = this.companyPermissions.filter(item => !item.IsDetached);
		this.companyPermissions.forEach(item => item.PrepareSave());

		this.encounters = this.encounters.filter(item => !item.IsDetached);
		this.encounters.forEach(item => item.PrepareSave());
	}

	/**
	 * Returns a character object for the given id
	 * @param id
	 */
	public GetCharacter(id: number): Character
	{
		if (!id)
		{
			return undefined;
		}

		return this.characters.find(item => item.characterId === id);
	}

	/**
	 * Returns an encounter object for the given id
	 * @param id
	 */
	public GetEncounter(id: number): Encounter
	{
		if (!id)
		{
			return undefined;
		}

		return this.encounters.find(item => item.encounterId === id);
	}
}
